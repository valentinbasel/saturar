#! /bin/bash
echo "----------------------------------------------------"
echo "| Programa de instalación para el proyecto SATURAR |"
echo "----------------------------------------------------"
read -p "Presiona ENTER para continuar."
#read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
#sudo dnf install -y  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
#sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

sudo apt update -y
sudo apt install -y python3-tk python3-pil python3-vlc build-essential 
sudo apt install -y pandoc  pkg-config libpoppler-cpp-dev python3-dev 
sudo apt install -y python3-pil.imagetk texlive-latex-base libglib2.0-dev
sudo apt install -y libgirepository1.0-dev libcairo2-dev vlc 
sudo apt install -y python3-setuptools python3-gtkspellcheck pip
sudo apt install -y python3-gi-cairo
sudo apt install -y python3-numpy



echo " vamos a instalar los  paquetes soportados por PIP"
#read -p "Presiona ENTER para continuar."
# read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key

pip uninstall -y saturar --break-system-packages 
pip install nltk networkx stop-words pygtkspellcheck --break-system-packages 
pip install pdftotext python-vlc --break-system-packages
pip install wordcloud --break-system-packages
pip install xlwt --break-system-packages
pip install pytz regex joblib tqdm pkgconfig --break-system-packages
pip install pandas==1.3.5 --break-system-packages
#pip install numpy==1.21.5 --break-system-packages
pip uninstall numpy  --break-system-packages
pip install matplotlib==3.5.1 --break-system-packages

#python -m pip install -e git+https://github.com/amueller/word_cloud#egg=wordcloud
echo " creamos el icono de escritorio"
#read -p "Presiona ENTER para continuar."
#read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
sudo cp iconos/saturar128.png /usr/share/icons/saturar128.png
sudo cp iconos/saturar.desktop /usr/share/applications/saturar.desktop
sudo cp iconos/saturar_wayland.desktop /usr/share/applications/saturar_wayland.desktop

echo " comenzamos con la instalación de SATURAR"
#read -p "Presiona ENTER para continuar."
#read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
sudo python3 setup.py install 
cd saturapi
sudo python3 setup.py install #--user

echo " llegamos al fin del instalador"
#read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
read -p "Presiona ENTER para continuar."
