# saturar



## Instalación

Saturar es un software 100% desarrollado con PYTHON, para poder instalarlo, es necesario descargar este repositorio en tu computadora, luego instalar una serie de paquetes extras que no son de python.

### Windows (10 en adelante)

Para instalar Saturar en windows 10 (o superior), primero debemos instalar miniconda (en caso de no estar instalado). Para eso podemos usar el archivo BAT:

 **install_miniconda_windows.bat** 

luego ejecutar con permisos de administrador el archivo BAT:

**install_windows10.bat**

### Fedora

abrir la carpeta donde se descargo y descomprimió el repositorio y ejecutar el script: install_fedora.sh

### Ubuntu

abrir la carpeta donde se descargo y descomprimió el repositorio y ejecutar el script: install_ubuntu.sh


## Como citar

Basel, V. (2024) Saturar 0.2.1 [Software de computador]. https://gitlab.com/valentinbasel/saturar/-/tree/0.2.1
