#! /bin/bash

echo "----------------------------------------------------"
echo "| Programa de instalación para el proyecto SATURAR |"
echo "----------------------------------------------------"
read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
sudo dnf install -y  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y python3-tkinter python3-pillow-tk python3-vlc python3-pip
sudo dnf install -y pandoc gcc-c++ pkgconfig poppler-cpp-devel python3-devel gtksourceview3-devel
echo " vamos a instalar los  paquetes soportados por PIP"
#read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
sudo pip uninstall -y saturar 
pip install nltk networkx stop-words  pygtkspellcheck 
pip install wordcloud
pip install  pdftotext python-vlc
pip install pytz   scipy 
pip install pandas==1.3.5 
pip install numpy==1.21.5
pip install matplotlib==3.5.1
#python -m pip install -e git+https://github.com/amueller/word_cloud#egg=wordcloud
echo " creamos el icono de escritorio"
#read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
sudo cp iconos/saturar128.png /usr/share/icons/saturar128.png
sudo cp iconos/saturar.desktop /usr/share/applications/saturar.desktop
sudo cp iconos/saturar_wayland.desktop /usr/share/applications/saturar_wayland.desktop

echo " comenzamos con la instalación de SATURAR"
#read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
sudo python3 setup.py install #--user
cd saturapi
sudo python3 setup.py install #--user

echo " llegamos al fin del instalador"
read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
