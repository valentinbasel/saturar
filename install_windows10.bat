cd %~dp0


call clonar.bat
REM pause
call activate saturar
REM echo " vamos a instalar los  paquetes soportados por PIP"



echo "Vamos a tratar de borrar Saturar en caso de que allá una instalación vieja"
pip uninstall -y saturar

echo " vamos a instalar los  paquetes soportados por PIP"
pip install nltk networkx stop-words wordcloud pygtkspellcheck
pip install  pdftotext python-vlc
pip install pytz
pip install pandas
pip install numpy
REM ==1.3.5
pip install matplotlib
REM ==3.5.1

echo "vamos a crear accesos directos"
set ICON_PATH=win\saturar.ico
set DEST_PATH=C:\Windows\System32
set SHORTCUT_PATH=win\Saturar_0.3.0.lnk
set START_MENU_PATH=%APPDATA%\Microsoft\Windows\Start Menu\Programs

echo Copiando acceso directo al menú de inicio...
copy "%SHORTCUT_PATH%" "%START_MENU_PATH%"
echo Copiando icono a la carpeta del sistema...

:: Copiar el archivo a la carpeta de Windows
copy "%ICON_PATH%" "%DEST_PATH%" 

python setup.py install
cd saturapi
python setup.py install
REM ::pip uninstall -y numpy



conda install -y conda-forge::adwaita-icon-theme conda-forge::python-graphviz 
REM numpy=1.21.5
echo " llegamos al fin del instalador"
pause
