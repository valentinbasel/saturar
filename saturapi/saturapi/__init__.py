#!/usr/bin/env python3
# -*- coding: utf-8 -*-


###############################################################################
# SATURAPI, API de conexión a la base de datos de saturar
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import sqlite3
#import gi
#gi.require_version("Gtk", "3.0")
#from gi.repository import Gtk, Gdk
import os
#from ..utils import DIALOGOS
from pathlib import Path
import time

class SATURAPI(object):

    """Docstring for SATURAPI. """

    def __init__(self, arch):
        """TODO: to be defined. """
        self.__con = None
        self.conectar(arch)
        self.Recuperar = RECUPERAR(self.__con)
        self.Buscar = BUSCAR(self.Recuperar)
        #self.Reporte = REPORTE(self.Recuperar)
        self.Agregar = AGREGAR(self.__con)
        self.Borrar = BORRAR(self.__con)

    def conectar(self, arch):
        """Levanta una instacia de la base de datos de saturar y la prepara 
           para poder trabajar con la API
        :returns: con

        """
        try:
            self.__con = sqlite3.connect(arch)
            print("la conexión a: ", arch, " fue exitosa")
            return True
        except Exception as e:
            print("hubo un error en la conexión")
            return False
            raise e

    def contar_elementos(self, lista):
        """
        Recibe una lista, y devuelve un diccionario con todas las repeticiones de
        cada valor
        """
        return {i: lista.count(i) for i in lista}


class RECUPERAR(object):

    """Docs tring for RECUPERAR. """

    def __init__(self,con):
        """TODO: to be defined. """
        self.__con = con

    def Paginas(self, id_cuaderno=None):
        """
        Recupero todas las paginas activas de un cuaderno.}
        entradas: Id del cuaderno
        retorna: lista con las paginas activas y sus datos en el formato:

        id_pagina
        id_cuaderno
        titulo
        contenido
        fecha_alt
        status

        """
        if self.__con is None:
            print("no ha iniciado una conexión con la base de datos")
            return False
        if id_cuaderno is None:
            cod = 'SELECT * FROM pagina'
        else:
            cod = 'SELECT * FROM pagina WHERE id_cuaderno = '+str(id_cuaderno)
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        cod_data = []
        for r in res:
            if r[5] == 1:
                tag = {'id_pagina': str(r[0]),
                       'id_cuaderno': str(r[1]),
                       'titulo': r[2],
                       'contenido': r[3],
                       'fecha_alt': r[4],
                       'status': str(r[5]),
                       'tipo': str(r[6])}
                cod_data.append(tag)
        return cod_data

    def Codigos(self):
        """
        """
        if self.__con is None:
            print("no ha iniciado una conexión con la base de datos")
            return False
        cod = 'SELECT * FROM codigo WHERE id_prop = 1'
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        cod_data = []
        for r in res:
            if r[6] == 1:
                tag = {'id_cod': str(r[0]),
                       'nombre_cod': r[1],
                       'color': r[2],
                       'memo': r[3],
                       'fecha_alt': r[5],
                       'status': str(r[6])}
                cod_data.append(tag)
        return cod_data

    def Todo_Codificado(self):
        """TODO: Docstring for recuperar_codificado.
        :returns: TODO

        """
        cod = 'SELECT * FROM codificacion'
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        cod_data = []
        for r in res:
            if r[8] == 1:
                tag = {'id_codificacion': str(r[0]),
                       'id_cod': r[1],
                       'id_prop': r[2],
                       'id_pagina': r[3],
                       'text_cod': r[4],
                       'fecha_creac': r[7],
                       'status': str(r[8])}
                cod_data.append(tag)
        return cod_data

    def Codificado(self, id_cod):
        """TODO: Docstring for recuperar_codificado.
        :returns: TODO

        """
        cod = 'SELECT * FROM codificacion WHERE id_cod = ' + id_cod
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        cod_data = []
        for r in res:
            if r[8] == 1:
                tag = {'id_codificacion': str(r[0]),
                       'id_cod': r[1],
                       'id_prop': r[2],
                       'id_pagina': r[3],
                       'text_cod': r[4],
                       'c_ini': r[5],
                       'c_fin': r[6],
                       'fecha_creac': r[7],
                       'status': str(r[8]),
                        'memo': str(r[9])}
                cod_data.append(tag)
        return cod_data

    def Pagina_Especifica(self, pagina):
        """TODO: Docstring for recuperar_pagina_especifica.

        :pagina: TODO
        :returns: TODO

        """
        if self.__con is None:
            print("no ha iniciado una conexión con la base de datos")
            return False
        cod = 'SELECT * FROM pagina WHERE id_pagina = '+str(pagina)
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        cod_data = []
        for r in res:
            if r[5] == 1:
                tag = {'id_pagina': str(r[0]),
                       'id_cuaderno': str(r[1]),
                       'titulo': r[2],
                       'contenido': r[3],
                       'fecha_alt': r[4],
                       'status': str(r[5]),
                       'tipo': str(r[6])
                       }
                cod_data.append(tag)
        return cod_data

    def Cuaderno_Especifico(self, cuad):
        """TODO: Docstring for recuperar_pagina_especifica.

        :pagina: TODO
        :returns: TODO

        """
        if self.__con is None:
            print("no ha iniciado una conexión con la base de datos")
            return False
        cod = 'SELECT * FROM cuaderno WHERE id_cuaderno = '+str(cuad)
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        cod_data = []
        for r in res:
            if r[5] == 1:
                tag = {'id_cuaderno': str(r[0]),
                       'id_proy': str(r[1]),
                       'nombre_cuad': r[2],
                       'memo': r[3],
                       'fecha_alt': r[4],
                       'status': str(r[5])}
                cod_data.append(tag)
        return cod_data

    def Categorias(self):
        """
        """
        if self.__con is None:
            print("no ha iniciado una conexión con la base de datos")
            return False
        cod = 'SELECT * FROM categoria WHERE id_prop = 1'
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        cod_data = []
        for r in res:
            if r[6] == 1:
                tag = {'id_cat': str(r[0]),
                       'nombre_cat': r[1],
                       'color': r[2],
                       'memo': r[3],
                       'fecha_alt': r[5],
                       'status': str(r[6])}
                cod_data.append(tag)
        return cod_data

    def Categorizado(self, id_cat):
        """TODO: Docstring for recuperar_codificado.
        :returns: TODO

        """
        cod = 'SELECT * FROM cod_axial WHERE id_cat = '+str(id_cat) 
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        cod_data = []
        for r in res:
            if r[4] == 1:
                tag = {'id_cod': str(r[1]),
                       'fecha_creac': r[3],
                       'status': str(r[4])}
                cod_data.append(tag)
        categor = []
        for c in cod_data:
            cod2 = 'SELECT nombre_cod, color, status FROM codigo WHERE id_cod = '+ str(c['id_cod'])
            cursorObj2 = self.__con.cursor()
            cursorObj2.execute(cod2)
            res2 = cursorObj2.fetchall()
            ca = {'id_cod': str(c['id_cod']), 'nombre':res2[0][0], 'color':res2[0][1]}
            print(res2)
            if res2[0][2]==1:
                categor.append(ca)
        return categor

class BUSCAR(object):
    """Docstring for BUSCAR. """

    def __init__(self,con):
        """TODO: to be defined. """
        self.s = con

    def o(self,a,b):
        """
        Devuelve los valores que esten en A o en B
        ej:
        a=[1,2,3,4]
        b=[3,4,5,6]
        o(a,b) = [1,2,3,4,5,6]
        """
        if type(a)==str:
            a = self.s.Codificado(a)
        if type(b)==str:
            b = self.s.Codificado(b)
        
        codf = a + b
        v = 0
        lista = []
        num=[]
        for dato in codf:
            if dato["text_cod"]  not in lista:
                lista.append(dato["text_cod"])
            else:
                num.append(v)
            v+=1
        for dato in num:
            codf.pop(dato)
        return codf

    def y(self,a,b):
        """
        Devuelve solo los valores que se encuentren en A y B al mismo tiempo
        ej:
        a=[1,2,3,4]
        b=[3,4,5,6]
        y(a,b) = [3,4]

        """
        if type(a)==str:
            a = self.s.Codificado(a)
        if type(b)==str:
            b = self.s.Codificado(b)
 
        #coda = self.s.Codificado(a)
        #codb = self.s.Codificado(b)
        res = []
        for d1 in a:
            for d2 in b:
                if d1["text_cod"] == d2["text_cod"]:
                    res.append(d1)
        return res

    def no(self,a,b):
        """
        Devuelve los valores de A que no esten en B
        ej:
        a=[1,2,3,4]
        b=[3,4,5,6]
        no(a,b) = [1,2]
        """
        if type(a)==str:
            a = self.s.Codificado(a)
        if type(b)==str:
            b = self.s.Codificado(b)
 
        #coda = self.s.Codificado(a)
        #codb = self.s.Codificado(b)
        res = []
        for d1 in a:
            band = False
            for d2 in b:
                if d1["text_cod"] == d2["text_cod"]:
                    band = True
            if band == False:
                res.append(d1)
        return res

# class REPORTE(object):

    # """Docstring for REPORTE. """

    # def __init__(self, saturapi):
        # """TODO: to be defined. """
        # self.s = saturapi

    # def Generar(self, codifi):
        # """
        # """
        # cods = """## Código: **{}**
# ### ID Código: {}
# ### ID Codificado: {}

# """
        # mkd_cod = """### Reporte ###

# Titulo cuaderno: **{}**

# Titulo página: **{}**

# tipo de registro: **{}**

# fecha de codificación: **{}**


# """
        # mkd_text = """Carácter inicial: **{}**

# Carácter final: **{}**

# ### Texto recuperado: ###
# {}

# ### Memo ###

# {}

# ---

# """
        # mkd_img = """Milisegundo inicial: **{}**

# Milisegundo final: **{}**

# ### Imagen: ###

# <img src="{}" alt=""
	# title="" width="400" />

# ### Transcripción: ###
# {}

# ### Memo ###

# {}

# ---

# """
        # arch = ""
        # for a in codifi:
            # datos = []
            # codigos = self.s.Codigos()
            # cod_cod=""
            # for _cod in codigos:
                # #print("cod: ",_cod["id_cod"])
                # #print("a: ",a["id_cod"])
                # if _cod["id_cod"].find(str(a["id_cod"]))>-1:
                    # #print("encontre el codigo!! ",_cod["id_cod"])
                    # cod_cod = cods.format(_cod["nombre_cod"],
                                          # _cod["id_cod"],
                                          # a["id_codificacion"])
                    # #print(cod_cod)

            # pag = self.s.Pagina_Especifica(str(a['id_pagina']))
            # cuad = self.s.Cuaderno_Especifico(str(pag[0]['id_cuaderno']))
            # datos.append(str(cuad[0]['nombre_cuad']) )
            # datos.append(str(pag[0]['titulo']))
            # tipo = str(pag[0]['tipo'])
            # datos.append(tipo)
            # fech = a['fecha_creac']
            # datos.append(fech)
            # cod_f = mkd_cod.format(*datos)
            # texto = a['text_cod']
            # memo = a['memo']
            # if tipo == "texto":
                # c_ini = str(a['c_ini'])
                # c_fin = str(a['c_fin'])
                # cod_c = mkd_text.format(c_ini,c_fin,texto,memo)
            # elif tipo == "multimedia":
                # c_ini = str(a['c_ini'])
                # c_fin = str(a['c_fin'])
                # direc = os.path.split(self.archivo)
                # im = direc[0] + "/img/" + str(a['id_codificacion'] + ".png")
                # cod_c = mkd_img.format(c_ini,c_fin,im,texto,memo)
            # arch = arch + cod_cod + cod_f + cod_c
        # v = VISOR_REPORTES(arch)


# class VISOR_REPORTES(object):

    # """Docstring for VISOR_REPORTES. """

    # def __init__(self, texto):
        # """TODO: to be defined. """
        # self.ruta = str(Path.home()) + "/"
        # self.ventana = Gtk.Window()
        # self.ventana.set_title("Visor de reporte")
        # self.ventana.resize(800,600)

        
        # #self.mensajes = DIALOGOS(self.ventana)
        # vbox = Gtk.VBox()

        # scrolltext = Gtk.ScrolledWindow()
        # scrolltext.set_hexpand(True)
        # scrolltext.set_vexpand(True)
        # frame = Gtk.Frame()
        # #frame.set_label("Resultado")
        # frame.set_shadow_type(1)
        # hbox = Gtk.HBox()
        # ######################################################################
        # #  configuraciones del textview
        # ######################################################################
        # # Widget con el textview donde se visualizan el texto a codificar
        # self.pagina = Gtk.TextView()
        # # Deshabilito la edición del textview
        # self.pagina.set_editable(False)
        # # Corto el texto en las palabras si se pasan del tamaño del textview
        # self.pagina.set_wrap_mode(Gtk.WrapMode.WORD)
        # self.p_buffer = self.pagina.get_buffer()
        # self.p_buffer.set_text(texto)
        # scrolltext.add(self.pagina)
        # frame.add(scrolltext)
        # boton_pdf = Gtk.Button("Exportar PDF")
        # boton_html = Gtk.Button("Exportar HTML")
        # boton_pdf.connect("clicked",self.boton_pdf_aceptar)
        # boton_html.connect("clicked",self.boton_html_aceptar)
        # hbox.pack_start(boton_pdf, True, True, 0)
        # hbox.pack_start(boton_html, True, True, 0)
        # self.ventana.connect("destroy", Gtk.main_quit)
        # vbox.pack_start(frame, True, True, 0)
        # vbox.pack_start(hbox, False, False, 0)
        # self.ventana.add(vbox)
        # self.ventana.show_all()
        # Gtk.main()

    # def obtener_text(self):
        # """TODO: Docstring for obtener_text.
        # :returns: TODO

        # """
        
        # texto = self.p_buffer.get_text(self.p_buffer.get_start_iter(),
                                        # self.p_buffer.get_end_iter(),
                                       # True)
        # return texto

    # def boton_html_aceptar(self, widget):
        # """TODO: Docstring for boton_pdf_aceptar.
        # :returns: TODO

        # """

        # print("preparo el html")
        # texto = self.obtener_text()
        # tmp = self.ruta+"temp.md"
        # archivo = open(tmp,"w")
        # archivo.writelines(texto)
        # archivo.close()
        # html = "pandoc "+tmp+" -o " +self.ruta+"reporte.html"
        # os.system(html)
        # #self.mensajes.informacion(texto = "el archivo fue creado correctamente",
        # #                            subtexto = "con el nombre 'reporte.html', cargado en el home")

    # def boton_pdf_aceptar(self, widget):
        # """TODO: Docstring for boton_pdf_aceptar.
        # :returns: TODO

        # """
        # texto = self.obtener_text()
        # tmp = self.ruta + "temp.md"
        # archivo = open(tmp, "w")

        # archivo.writelines(texto)
        # archivo.close()
        # html = "pandoc "+tmp+" -o"  +self.ruta+"temp.html"

        # os.system(html)
        # pdf =  "pandoc " + self.ruta + "temp.html" + " -o ~/reporte.pdf"
        # os.system(pdf)
        # #os.system("rm ~temp.md")
        # #self.mensajes.informacion(texto = "el archivo fue creado correctamente",
        # #                            subtexto = "con el nombre 'reporte.pdf', cargado en el home")

class AGREGAR(object):

    """Docstring for AGREGAR. """

    def __init__(self,con):
        """TODO: to be defined. """
        self.__con = con

    def codificado(self,id_cod,id_prop,id_pagina,text_cod,c_ini,c_fin):
        """TODO: Docstring for codificado.
        :returns: TODO
        id_codificacion
        id_cod
        id_prop
        id_pagina
        text_cod
        c_ini
        c_fin
        fecha_creac
        status
        memo
        """
        if self.__con is None:
            print("no ha iniciado una conexión con la base de datos")
            return False
        cod_tam = ('SELECT MAX(id_codificacion) FROM codificacion')
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod_tam)
        tam = cursorObj.fetchall()
        if tam[0][0] is not None:
            id_codificado = int(tam[0][0])+1
        else:
            id_codificado = 1
        fecha_alt = time.strftime("%d/%m/%y"),
        status = 1
        memo=""
        n_cod = [int(id_codificado),
                int(id_cod),
                int(id_prop),
                int(id_pagina),
                str(text_cod),
                str(c_ini),
                str(c_fin),
                str(fecha_alt),
                int(status),
                str(memo)
                ]
        print(n_cod)
        cursorObj.execute('INSERT INTO codificacion(id_codificacion,id_cod,id_prop,id_pagina,text_cod,c_ini,c_fin,fecha_alt,status,memo) VALUES(?,?,?,?,?,?,?,?,?,?)', n_cod)
        self.__con.commit()


    def codigo(self, nombre_cod, color="#ff0000", memo="", id_prop=1 ):
        """TODO: Docstring for codigos.
        nombre_cod: str
        color: str (#RRGGBB)
        memo: str (por defecto " ")
        id_prop: int (por defecto 1)
        :returns: TODO

        """
        if self.__con is None:
            print("no ha iniciado una conexión con la base de datos")
            return False
        cod_tam = ('SELECT MAX(id_cod) FROM codigo')
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod_tam)
        tam = cursorObj.fetchall()
        if tam[0][0] is not None:
            id_cod = int(tam[0][0])+1
        else:
            id_cod = 1
        fecha_alt = time.strftime("%d/%m/%y"),
        status = 1
        n_cod = [int(id_cod),
                 nombre_cod,
                 color,
                 memo,
                 int(id_prop),
                 fecha_alt[0],
                 int(status)]

        cursorObj.execute('INSERT INTO codigo(id_cod, nombre_cod, color,memo, id_prop, fecha_alt, status) VALUES(?, ?, ?, ?,?,? ,?)', n_cod)
        self.__con.commit()
        return int(id_cod)

    def pagina(self, id_cuaderno,titulo,contenido,tipo="texto"):
        """TODO: Docstring for pagina.

        :id_cuad: Identificador unico del cuaderno (str)
        :titulo: Nombre que va a tener la página (str)
        :contenido: texto a cargar en el cuerpo de la página (str)
        :tipo: valores 'texto' / 'multimedia'. default: 'texto' (str)
        :returns: True
        """
        if self.__con is None:
            print("no ha iniciado una conexión con la base de datos")
            return False
        cod_tam = ('SELECT MAX(id_pagina) FROM pagina')
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod_tam)
        tam = cursorObj.fetchall()
        if tam[0][0] is not None:
            id_pagina = int(tam[0][0])+1
        else:
            id_pagina = 1
        fecha_alt = time.strftime("%d/%m/%y"),
        status = 1
        n_pag = [int(id_pagina),
                 int(id_cuaderno),
                 titulo,
                 contenido,
                 fecha_alt[0],
                 int(status),
                 tipo]
        cursorObj.execute('INSERT INTO pagina(id_pagina, id_cuaderno, titulo, contenido, fecha_alt, status,tipo) VALUES( ?, ?, ?,?,? ,?,?)', n_pag)
        self.__con.commit()
        return True

    def categoria(self, nombre, color="#ff0000", memo="", id_prop=1):
        """TODO: Docstring for categoria.
        nombre: str
        color: str (#RRGGBB)
        memo: str (por defecto " ")
        id_prop: int (por defecto 1)
        :returns: TODO

        """
        if self.__con is None:
            print("no ha iniciado una conexión con la base de datos")
            return False
        cod_tam = ('SELECT MAX(id_cat) FROM categoria')
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod_tam)
        tam = cursorObj.fetchall()
        if tam[0][0] is not None:
            id_cod = int(tam[0][0])+1
        else:
            id_cod = 1
        fecha_alt = time.strftime("%d/%m/%y"),
        status = 1
        n_cod = [int(id_cod),
                 nombre,
                 color,
                 memo,
                 int(id_prop),
                 fecha_alt[0],
                 int(status)]

        cursorObj.execute('INSERT INTO categoria(id_cat, nombre_cat, color,memo, id_prop, fecha_alt, status) VALUES(?, ?, ?, ?,?,? ,?)', n_cod)
        self.__con.commit()

    def cuaderno(self,titulo,contenido,id_proy=1):
        """TODO: Docstring for cuaderno.
        titulo: string
        contenido: string
        id_proy: int (por deafult 1)
        :returns: TODO

        """
        cod_tam = ('SELECT MAX(id_cuaderno) FROM cuaderno')
        cursorObj = self.__con.cursor()
        cursorObj.execute(cod_tam)
        tam = cursorObj.fetchall()
        if tam[0][0] is not None:
            id_cuaderno = int(tam[0][0])+1
        else:
            id_cuaderno = 1
        fecha_alt = time.strftime("%d/%m/%y"),
        status = 1
        n_cod = [int(id_cuaderno),
                 int(id_proy),
                 titulo,
                 contenido,
                 fecha_alt[0],
                 int(status)]
        cursorObj.execute('INSERT INTO cuaderno(id_cuaderno, id_proy, nombre_cuad, memo, fecha_alt, status) VALUES( ?, ?, ?,?,? ,?)', n_cod)
        self.__con.commit()
 
class BORRAR(object):

    """Docstring for BORRAR. """

    def __init__(self,con):
        """TODO: to be defined. """
        self.__con = con

    def codigo(self, cod_id):
        """TODO: Docstring for codigo.
        :returns: TODO

        """
        if cod_id != "0":
            cod = 'UPDATE codigo SET status = 0 where id_cod = '+ cod_id
            cursorObj = self.__con.cursor()
            cursorObj.execute(cod)
            self.__con.commit()
            cod = 'UPDATE cod_axial SET status = 0 where id_cod = '+ cod_id
            cursorObj = self.__con.cursor()
            cursorObj.execute(cod)
            self.__con.commit()
            cod = 'UPDATE codificacion SET status = 0 where id_cod = '+ cod_id
            cursorObj = self.__con.cursor()
            cursorObj.execute(cod)
            self.__con.commit()
            

