from distutils.core import setup

setup(name="saturapi",  # Nombre
    version="0.2",  # Versión de desarrollo
    description="API para el software para analisis QDA Saturar",  # Descripción del funcionamiento
    author="Valentín Basel",  # Nombre del autor
    author_email='valentinbasel@gmail.com',  # Email del autor
    license="GPL 3.0",  # Licencia: MIT, GPL, GPL 2.0...
    url="http://satur.ar",  # Página oficial (si la hay)
    packages= ['saturapi'],#,"saturapi/saturapi"], #['src','src/img','src/reportes'],#
    package_data = { '': ['*.py'],}, 
)
