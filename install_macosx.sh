#! /bin/bash
echo "----------------------------------------------------"
echo "| Programa de instalación para el proyecto SATURAR |"
echo "----------------------------------------------------"
read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
conda install -y -c conda-forge --file mac.yaml
echo " vamos a instalar los  paquetes soportados por PIP"
#read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
pip install nltk networkx stop-words pygtkspellcheck
pip install wordcloud
pip install pdftotext python-vlc
pip install pytz scipy #  scipy creo que esta lib esta al pedo
pip install pandas #==1.3.5
pip install numpy #==1.21.5
pip install matplotlib #==3.5.1
#python -m pip install -e git+https://github.com/amueller/word_cloud#egg=wordcloud
echo " creamos el icono de escritorio"
#read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
sudo cp iconos/saturar128.png /usr/share/icons/saturar128.png
sudo cp iconos/saturar.desktop /usr/share/applications/saturar.desktop
sudo cp iconos/saturar_wayland.desktop /usr/share/applications/saturar_wayland.desktop

conda install -y conda-forge::adwaita-icon-theme conda-forge::python-graphviz 

echo " comenzamos con la instalación de SATURAR"

#read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
sudo python3 setup.py install #--user
cd saturapi
sudo python3 setup.py install #--user

echo " llegamos al fin del instalador"
read -n 1 -r -s -p "Presiona cualquier tecla para continuar..." key
