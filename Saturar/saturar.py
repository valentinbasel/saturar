#!/usr/bin/env python3
# -*- coding: utf-8 -*-


###############################################################################
# SATURAR, software para codificación y analisis basado en 
# teoría fundamentada
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

## LIBRERIAS ESTANDARES 
import gi
import time
import sqlite3
import os
import sys
import importlib.util
import pathlib
import platform
from threading import Thread
from time import sleep
import logging



## LIBRERIAS GTK3
gi.require_version("Gtk", "3.0")
gi.require_version('PangoCairo', '1.0')
gi.require_version('Gdk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf

## MODULOS SATURAR
from .utils import DIALOGOS
from .codificar import CODIFICAR
from .codigos import CODIGOS
from .paginas import PAGINAS
from .cuadernos import CUADERNOS
from .categorias import CATEGORIA
from .categorizacion import CATEGORIZAR
from .nueva_proyecto import NUEVO_PROYECTO

from .img import IMG
from .buscador import BUSCADOR
from .creditos import Info


class Splash(Thread):

    def __init__(self):
        super(Splash, self).__init__()
        ruta_s = os.path.dirname(os.path.abspath( __file__ ))
        self.window = Gtk.Window(Gtk.WindowType.POPUP)
        self.window.set_position(Gtk.WindowPosition.CENTER)
        self.window.set_default_size(400, 250)
        self.window.set_resizable(False)
        self.window.set_transient_for(None)
        self.window.set_destroy_with_parent(True)
        self.window.set_skip_taskbar_hint(True)
        self.window.set_skip_pager_hint(True)
        self.window.set_type_hint(Gdk.WindowTypeHint.SPLASHSCREEN)
        self.window.set_decorated(False)
        box = Gtk.Box()
        img = Gtk.Image.new_from_file(ruta_s + "/img/inicial.png")
        box.pack_start(img, True, True, 0)
        self.window.add(box)
        self.window.set_auto_startup_notification(False)
        self.window.show_all()
        self.window.set_auto_startup_notification(True)
        while Gtk.events_pending():
            Gtk.main_iteration()



class SATURAR(object):

    """Clase central de SATURAR. con el manejador de archivos y carga """

    def __init__(self):
        """TODO: to be defined. """
        #######################################################################
        # Variables globales del proyecto
        #######################################################################
        home = os.path.expanduser("~") + "/.config/"
        ruta = "saturar/"
        pl = "plugins/"
        r_fin=home + ruta + 'saturar.log'
        logging.basicConfig(filename=r_fin,
                            format='%(asctime)s %(message)s', 
                            datefmt='%m/%d/%Y %I:%M:%S %p',
                            encoding='utf-8', 
                            level=logging.DEBUG)
        self.proyecto = ""
        self.id_cuad = 1
        ruta_s = os.path.dirname(os.path.abspath( __file__ ))
        self._os = platform.system()
        if self._os=='Linux':


            import vlc
            self.vlcinstance = vlc.Instance("--no-xlib")
        #######################################################################
        # inicio la ventana central
        #######################################################################
        version = Info.version#"0.2"
        nombre = "versión " + version
        self.ventana = Gtk.Window()
        self.ventana.set_title(nombre)
        self.ventana.set_default_icon_from_file(ruta_s + "/img/icon.png")
        self.ventana.maximize()
        hbox_header = Gtk.HBox()
        self.cuaderno = None
        self.mensajes = DIALOGOS(self.ventana)
        # "accessories-calculator" 
        #######################################################################
        # Creo el boton de menu principal con su popover
        #######################################################################
        boton_menu = Gtk.Button()#.new_from_icon_name("open-menu-symbolic", 1)
        menu_img = Gtk.Image.new_from_file(ruta_s + "/img/menu.svg")
        #boton_menu.set_image(menu_img)
        grid = Gtk.Grid ()
        label = Gtk.Label ('Menu')
        grid.attach (menu_img, 0, 0, 1, 1)
        grid.attach (label, 0, 1, 1, 1)
        grid.show_all ()
        boton_menu.add(grid)
        popover = Gtk.PopoverMenu.new()
        popover.set_relative_to(boton_menu)
        boton_menu.connect("clicked", self.boton_click, popover)
        pbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        popover.add(pbox)
        nuevo = Gtk.ModelButton.new()
        nuevo.set_property('margin', 10)
        nuevo.set_label("Nuevo proyecto")
        nuevo.connect("clicked", self.nuevo_proyecto)

        abrir_p = Gtk.ModelButton.new()
        abrir_p.set_property('margin', 10)
        abrir_p.set_label("Abrir proyecto")
        abrir_p.connect("clicked", self.abrir_proyecto)
        
        about_b = Gtk.ModelButton.new()
        about_b.set_property('margin', 10)
        about_b.set_label("Acerca de")
        about_b.connect("clicked", self.acerca_de)

        cerrar_p = Gtk.ModelButton.new()

        cerrar_p.set_property('margin', 10)
        cerrar_p.set_label("Salir")
        cerrar_p.connect("clicked", self.salir)
        pbox.pack_start(nuevo, False, False, 0)
        pbox.pack_start(abrir_p, False, False, 0)
        pbox.pack_start(about_b, False, False, 0)
        pbox.pack_start(cerrar_p, False, False, 0)
        hbox_header.pack_start(boton_menu, True, True, 0)
        #######################################################################

        #######################################################################
        # Creo el boton de para operaciones con su popover
        #######################################################################
        self.boton_menu_c = Gtk.Button()
        self.boton_menu_c.set_sensitive(False)
        print(ruta_s + "/img/reportes.png")
        menu_c_img = Gtk.Image.new_from_file(ruta_s + "/img/reportes.svg")
        grid = Gtk.Grid ()
        label = Gtk.Label ('Reportes')
        grid.attach (menu_c_img, 0, 0, 1, 1)
        grid.attach (label, 0, 1, 1, 1)
        grid.show_all ()
        self.boton_menu_c.add(grid)
        popover_c = Gtk.PopoverMenu.new()
        popover_c.set_relative_to(self.boton_menu_c)
        self.boton_menu_c.connect("clicked", self.boton_click_c, popover_c)
        pbox_c = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        popover_c.add(pbox_c)
        # Con esta función cargo de la carpeta plugin
        # todos los reportes que esten guardados como plugins 
        directorio_actual = pathlib.Path(__file__).parent.absolute()
        self.cargador_plugin(pbox_c,
                             DIALOGOS,
                             directorio_actual,'/reportes/')
        hbox_header.pack_start(self.boton_menu_c, True, True, 0)

        #######################################################################

        #######################################################################
        # Creo el boton de para operaciones con su popover
        #######################################################################
        self.boton_menu_herr = Gtk.Button()
        self.boton_menu_herr.set_sensitive(False)
        print(ruta_s + "/img/herramientas.png")

        menu_herr_img = Gtk.Image.new_from_file(ruta_s + "/img/herramientas.svg")
        grid = Gtk.Grid ()
        label_herr = Gtk.Label ('Herramientas')
        grid.attach (menu_herr_img, 0, 0, 1, 1)
        grid.attach (label_herr, 0, 1, 1, 1)
        grid.show_all ()
        self.boton_menu_herr.add(grid)
        popover_herr = Gtk.PopoverMenu.new()
        popover_herr.set_relative_to(self.boton_menu_herr)
        self.boton_menu_herr.connect("clicked", self.boton_click_c, popover_herr)
        pbox_herr = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        popover_herr.add(pbox_herr)
        # Con esta función cargo de la carpeta plugin
        # todos los reportes que esten guardados como plugins 
        directorio_actual = pathlib.Path(__file__).parent.absolute()
        self.cargador_plugin(pbox_herr,
                             DIALOGOS,
                             directorio_actual,'/herramientas/')
        hbox_header.pack_start(self.boton_menu_herr, True, True, 0)

        #######################################################################
        # Creo el boton de para operaciones con su popover
        #######################################################################
        self.boton_menu_plug = Gtk.Button()
        self.boton_menu_plug.set_sensitive(False)
        menu_plug_img = Gtk.Image.new_from_file(ruta_s + "/img/plugin.svg")
        grid = Gtk.Grid ()
        label = Gtk.Label ('Plugins')
        grid.attach (menu_plug_img, 0, 0, 1, 1)
        grid.attach (label, 0, 1, 1, 1)
        grid.show_all ()
        self.boton_menu_plug.add(grid)
        popover_plug = Gtk.PopoverMenu.new()
        popover_plug.set_relative_to(self.boton_menu_plug)
        self.boton_menu_plug.connect("clicked",
                                     self.boton_click_c,
                                     popover_plug)
        pbox_plug = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        popover_plug.add(pbox_plug)
        # Con esta función cargo de la carpeta plugin
        # todos los reportes que esten guardados como plugins 
        home = os.path.expanduser("~") + "/.config/"
        ruta = "saturar/"
        r_fin=home + ruta
        directorio_actual = r_fin
        self.cargador_plugin(pbox_plug,
                             DIALOGOS,
                             directorio_actual,'/plugins/')
        hbox_header.pack_start(self.boton_menu_plug, True, True, 0) 
        #######################################################################
        # Creo el boton de para busqueda con su popover
        #######################################################################
        self.boton_menu_d = Gtk.Button()
        self.boton_menu_d.set_sensitive(False)
        menu_d_img = Gtk.Image.new_from_file(ruta_s + "/img/buscar.svg")
        grid = Gtk.Grid ()
        label = Gtk.Label ('Buscar')
        grid.attach (menu_d_img, 0, 0, 1, 1)
        grid.attach (label, 0, 1, 1, 1)
        grid.show_all ()
        self.boton_menu_d.add(grid)
        popover_d = Gtk.PopoverMenu.new()
        popover_d.set_relative_to(self.boton_menu_d)
        self.boton_menu_d.connect("clicked", self.boton_click_d, popover_d)
        pbox_d = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        popover_d.add(pbox_d)
        operd1 = Gtk.ModelButton.new()
        operd1.set_property('margin', 10)
        operd1.set_label("Buscar")
        operd1.connect("clicked", self.buscador)
        pbox_d.pack_start(operd1, False, False, 0)
        hbox_header.pack_start(self.boton_menu_d, True, True, 0)
        ######################################################################
        self.ventana.connect("destroy", Gtk.main_quit)
        headerbar = Gtk.HeaderBar()
        headerbar.set_title("SATURAR")
        headerbar.set_subtitle(version)
        headerbar.set_show_close_button(True)
        headerbar.add(hbox_header)
        self.ventana.set_titlebar(headerbar)
        self.paginas = Gtk.Notebook()
        self.paginas.set_tab_pos(Gtk.PositionType.LEFT)
        ######################################################################
        # crear paginas
        ######################################################################
        self.caja_cuaderno = Gtk.HBox()
        cuad_h = Gtk.HBox()
        cuad_lab = Gtk.Label("Cuadernos")
        cuad_img = Gtk.Image.new_from_file(ruta_s + "/img/cuaderno.svg")
        cuad_h.pack_start(cuad_img, True, True, 10)
        cuad_h.pack_start(cuad_lab, True, True, 0)
        self.p_cuaderno = Gtk.Box()
        self.p_cuaderno.set_border_width(5)
        self.p_cuaderno.add(self.caja_cuaderno)
        self.paginas.append_page(self.p_cuaderno, cuad_h)
        cuad_h.show_all()
        pag_h = Gtk.HBox()
        pag_lab = Gtk.Label("Paginas")
        pag_img = Gtk.Image.new_from_file(ruta_s + "/img/paginas.svg")
        pag_h.pack_start(pag_img, True, True, 10)
        pag_h.pack_start(pag_lab, True, True, 0)
        self.caja_pagina = Gtk.HBox()
        self.p_paginas = Gtk.Box()
        self.p_paginas.set_border_width(5)
        self.p_paginas.add(self.caja_pagina)
        self.paginas.append_page(self.p_paginas, pag_h)
        pag_h.show_all()
        cod_h = Gtk.HBox()
        cod_lab = Gtk.Label("Códigos")
        cod_img = Gtk.Image.new_from_file(ruta_s + "/img/codigos.svg")
        cod_h.pack_start(cod_img, True, True, 10)
        cod_h.pack_start(cod_lab, True, True, 0)
        self.caja_cod = Gtk.HBox()
        self.page_cod = Gtk.Box()
        self.page_cod.set_border_width(5)
        self.page_cod.add(self.caja_cod)
        self.paginas.append_page(self.page_cod, cod_h)
        cod_h.show_all()
        codi_h = Gtk.HBox()
        codi_lab = Gtk.Label("Codificar")
        codi_img = Gtk.Image.new_from_file(ruta_s + "/img/codificar.svg")
        codi_h.pack_start(codi_img, True, True, 10)
        codi_h.pack_start(codi_lab, True, True, 0)
        self.p_codific = Gtk.Box()
        self.p_codific.set_border_width(5)
        self.caja_codific = Gtk.HBox()
        self.p_codific.add(self.caja_codific)
        self.paginas.append_page(self.p_codific, codi_h)
        codi_h.show_all()
        cat_h = Gtk.HBox()
        cat_lab = Gtk.Label("Categorias")
        cat_img = Gtk.Image.new_from_file(ruta_s + "/img/categorias.svg")
        cat_h.pack_start(cat_img, True, True, 10)
        cat_h.pack_start(cat_lab, True, True, 0)
        self.p_categoria = Gtk.Box()
        self.p_categoria.set_border_width(5)
        self.caja_categoria = Gtk.HBox()
        self.p_categoria.add(self.caja_categoria)
        self.paginas.append_page(self.p_categoria, cat_h)
        cat_h.show_all()
        cate_h = Gtk.HBox()
        cate_lab = Gtk.Label("Categorizar")
        cate_img = Gtk.Image.new_from_file(ruta_s + "/img/categorizar.svg")
        cate_h.pack_start(cate_img, True, True, 10)
        cate_h.pack_start(cate_lab, True, True, 0)
        self.p_cod_ax = Gtk.Box()
        self.p_cod_ax.set_border_width(5)
        self.caja_cod_ax = Gtk.HBox()
        self.p_cod_ax.add(self.caja_cod_ax)
        self.paginas.append_page(self.p_cod_ax, cate_h)
        cate_h.show_all()
        self.ventana.add(self.paginas)
        self.paginas.connect("switch-page", self.boton_pagina)
        self.paginas.set_show_tabs(False)
        self.ventana.show_all()

    def acerca_de(self,arg1):
        """TODO: Docstring for acerca_de.
        :returns: TODO

        """
        creditos = Info()
        about = Gtk.AboutDialog()
        ruta_s = os.path.dirname(os.path.abspath(__file__))
        ruta = ruta_s+"/img/saturar.svg"
        logo = GdkPixbuf.Pixbuf.new_from_file_at_size(ruta, 250, 250)
        about.set_logo(logo)
        about.set_name(creditos.name)
        about.set_program_name("")
        about.set_authors(creditos.authors)
        about.set_documenters(creditos.documenters)
        about.set_artists(creditos.artists)
        about.set_translator_credits(creditos.translator)
        about.set_version(creditos.version)
        about.set_comments(creditos.description)
        about.set_copyright(creditos.copyright)
        about.set_website(creditos.website)
        about.set_license(creditos.license)
        about.set_wrap_license(True)
        about.run()
        about.destroy()

    def cargador_plugin(self,
                        pbox_c,
                        dialogos,
                        directorio_actual,
                        direc):
        """TODO: Docstring for cargador_plugin.
        :returns: TODO
        """
        s = Splash()
        dir_plugin = str(directorio_actual) + direc
        contenido = os.listdir(dir_plugin)
        for pl in contenido:
            direc = dir_plugin + pl
            if os.path.isdir(direc):
                MODULE_PATH = dir_plugin + pl + "/plugin.py"
                MODULE_NAME = ""
                logging.info(MODULE_PATH)
                spec = importlib.util.spec_from_file_location(MODULE_NAME, 
                                                              MODULE_PATH)
                modulevar = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(modulevar)
                plug = modulevar.PLUGIN(dialogos)
 
                oper = Gtk.ModelButton.new()
                oper.set_property('margin', 10)
                if plug.band == True:
                    oper.set_label(plug.etiqueta)
                    oper.connect("clicked", self.ejecutar_plugin,plug)
                else:
                    s.window.hide()
                    cad1 = "se encontro el siguiente error en el plugin "
                    cad = cad1 + plug.etiqueta + " :\n " + str(plug.err)
                    self.mensajes.error(cad)
                    logging.error(cad)
                    oper.set_label(plug.etiqueta+" -->error")
                    oper.set_sensitive(False)
                    #s.window.hide() 
                pbox_c.pack_start(oper, False, False, 0)
        s.window.hide()

    def ejecutar_plugin(self,widget,plug):
        """TODO: Docstring for ejecutar_plugin(.
        :returns: TODO

        """
        r ,t= plug.ejecutar(self.arch)
        if r==False:
            self.mensajes.error(t)

    # ver para agregarlo como un plugin    
    # def ide(self,widget):
        # """TODO: Docstring for ide.
        # :returns: TODO

        # """
        # # /home/vbasel/virtualenv/Saturar/saturar/Saturar/ide
        # # /home/vbasel/virtualenv/Saturar/saturar/saturar/ide/main.py
        # cad = "python3 "+sys.path[0]+"/Saturar/ide/main.py "+self.arch #+" &"
        # print(cad)
        # os.system(cad)

    def salir(self, widget):
        """TODO: Docstring for salir.

        :widget: TODO
        :returns: TODO

        """
        Gtk.main_quit()

    def nuevo_proyecto(self, widget):
        """TODO: Docstring for nuevo_proyecto.
        :returns: TODO

        """
        NP = NUEVO_PROYECTO(self)

    def abrir_proyecto(self, widget):
        """TODO: Docstring for nuevo_proyecto.

        :widget: TODO
        :returns: TODO

        """
        dialog = Gtk.FileChooserDialog(
                    title="Seleccione un archivo SQLite con la base de datos SATURAR", 
                    parent=self.ventana,
                    action=Gtk.FileChooserAction.OPEN
                    )
        dialog.add_buttons(
                            Gtk.STOCK_CANCEL,
                            Gtk.ResponseType.CANCEL,
                            Gtk.STOCK_OPEN,
                            Gtk.ResponseType.OK,)
        filter_pdf = Gtk.FileFilter()
        filter_pdf.set_name("Archivos sqlite")
        filter_pdf.add_pattern("*.db")
        dialog.add_filter(filter_pdf)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            arch = dialog.get_filename()
            self.cargar(arch)
        elif response == Gtk.ResponseType.CANCEL:
            pass
        dialog.destroy()

    def cargar(self, arch):
        """TODO: Docstring for cargar.

        :arg1: TODO
        :returns: TODO

        """
        self.arch = arch
        self.con = sqlite3.connect(arch)
        self.page_cod.remove(self.caja_cod)
        self.caja_cod = Gtk.HBox()
        self.page_cod.add(self.caja_cod)
        CODIGOS(1, self.con, self.caja_cod, self.arch)
        self.p_paginas.remove(self.caja_pagina)
        self.caja_pagina = Gtk.HBox()
        self.p_paginas.add(self.caja_pagina)
        self.t_paginas = PAGINAS(self.id_cuad,
                                 self.con,
                                 self.caja_pagina,
                                 self.ventana)
        self.p_categoria.remove(self.caja_categoria)
        self.caja_categoria = Gtk.HBox()
        self.p_categoria.add(self.caja_categoria)
        self.t_categoria = CATEGORIA(self.id_cuad,
                                     self.con,
                                     self.caja_categoria)
        self.p_cuaderno.remove(self.caja_cuaderno)
        self.caja_cuaderno = Gtk.HBox()
        self.p_cuaderno.add(self.caja_cuaderno)
        self.cuaderno = CUADERNOS(1,
                                  self.con,
                                  self.caja_cuaderno,
                                  self.t_paginas)
        self.p_cod_ax.remove(self.caja_cod_ax)
        self.caja_cod_ax = Gtk.HBox()
        self.p_cod_ax.add(self.caja_cod_ax)
        self.categorizar = CATEGORIZAR(1,
                                       self.con,
                                       self.caja_cod_ax)
        self.paginas.set_show_tabs(True)
        self.boton_menu_c.set_sensitive(True)
        self.boton_menu_plug.set_sensitive(True)
        self.boton_menu_herr.set_sensitive(True)
        self.boton_menu_d.set_sensitive(True)

    def boton_click(self, button, popover):
        if popover.get_visible():
            popover.hide()
        else:
            popover.show_all()

    def boton_click_c(self, button, popover):
        if popover.get_visible():
            popover.hide()
        else:
            popover.show_all()

    def boton_click_d(self, button, popover):
        if popover.get_visible():
            popover.hide()
        else:
            popover.show_all()

    def buscador(self, arg1):
        """TODO: Docstring for buscador.

        :arg1: TODO
        :returns: TODO

        """
        c = BUSCADOR(self.arch,self)

    def buscar(self, id_p, caracter):
        """TODO: Docstring for buscar.

        :arg1: TODO
        :returns: TODO

        """
        self.paginas.set_current_page(3)
        self.p_codific.remove(self.caja_codific)
        self.caja_codific = Gtk.HBox()
        id_prop = 1
        self.c = CODIFICAR(id_p,
                           id_prop,
                           self.con,
                           self.caja_codific,
                           self.ventana)
        self.p_codific.add(self.caja_codific)
        self.c.entry_salt.set_text(str(caracter))
        self.ventana.set_focus(self.c.bot_salt)
        self.c.bot_salt.activate()

    def boton_pagina(self, a, b, c):
        """ C == valor entero que representa cada pagina empensando por
        el numero 0
        :returns: TODO

        """
        if self.paginas.get_current_page() == -1:
            return False
        if c == 1 and self.cuaderno is not None:
            txt = "Debes elegir un cuaderno antes."
            stxt = "se seleccionara el primer cuaderno de la lista"
            if self.cuaderno.id_cuad is None:
                self.mensajes.error(texto=txt,
                                    subtexto=stxt)
                self.paginas.set_current_page(0)
            else:
                self.id_cuad = self.cuaderno.obtener_id_cuad()
                self.p_paginas.remove(self.caja_pagina)
                self.caja_pagina = Gtk.HBox()
                self.p_paginas.add(self.caja_pagina)
                self.t_paginas = PAGINAS(self.id_cuad, 
                                         self.con, 
                                         self.caja_pagina,
                                         self.ventana)
        if c == 2:
            pass
        if c == 3:
            self.p_codific.remove(self.caja_codific)
            self.caja_codific = Gtk.HBox()
            id_pag = self.t_paginas.obtener_id_pag()
            tipo_pag = self.t_paginas.obtener_tipo_pag()
            id_prop = 1
            if tipo_pag == "texto":
                self.c = CODIFICAR(id_pag, 
                                   id_prop, 
                                   self.con,
                                   self.caja_codific,
                                   self.ventana)
                self.p_codific.add(self.caja_codific)
            elif tipo_pag == "multimedia" and self._os == "Linux":
                from .multimedia import VideoPlayer
                self.c =  VideoPlayer(self.con,
                                      id_prop,
                                      id_pag,
                                      self.caja_codific,
                                      self.arch,
                                      self.vlcinstance,
                                      self.ventana)
                self.c.main()
                self.p_codific.pack_start(self.caja_codific,True, True,0)
            elif tipo_pag == "imagen":
                self.c =  IMG(self.con,
                                      id_prop,
                                      id_pag,
                                      self.caja_codific,
                                      self.arch,
                                      self.ventana)
                self.c.main()
                self.p_codific.pack_start(self.caja_codific,True, True,0)
        if c == 5:
            pass
            self.categorizar.actualizar_tabla(None)
            self.categorizar.actualizar_tabla_cat_cod(None)
            self.categorizar.actualizar_tabla_cod(None)


