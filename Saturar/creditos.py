#!/usr/bin/env python
# -*- coding: utf-8 -*-:w

# Creamos una clase que almacena la información del programa (después se usara)


class Info:
    name = "Saturar"
    version = "0.3.0"
    copyright = "Copyright © 2019 Valentin Basel."
    authors = ["Valentin Basel - valetinbasel@gmail.com"]
    documenters = ["Agustin Zanotti - azanotti@unvm.edu.ar","Paula Gabriela Nuñez - pnunez@unrn.edu.ar"]

    artists = ["Luis Britos"]
    translator = "todavia sin traductores"
    website = "http://satur.ar/"
    description = "Software para analisis de datos cualitativos"
    license = "This program is free software; you can redistribute it and/or \
modify it under the terms of the GNU General Public License as published by \
the Free Software Foundation; either version 3 of the License, or (at your \
option) any later version. \n\nThis program is distributed in the hope that \
it will be useful, but WITHOUT ANY WARRANTY; without even the implied \
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. \
See the GNU General Public License for more details. \n\nYou should have \
received a copy of the GNU General Public License along with this program; \
if not, write to the Free Software Foundation, Inc., 51 Franklin Street, \
Fifth Floor, Boston, MA 02110-1301, USA."
