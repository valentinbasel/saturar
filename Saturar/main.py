#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# cargador de saturar
# Copyright © 2020 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

import gi
import time
import os
import errno
import sys

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GObject
lib_path = os.path.abspath(os.path.join(__file__, '..', '..', '..', 'lib'))
sys.path.append(lib_path)
from .saturar import SATURAR


def pl_conf():
    """TODO: Docstring for pl_conf.
    :returns: TODO

    """
    home = os.path.expanduser("~") + "/.config/"
    if os.path.exists(home)==False:
        try:
            os.mkdir(home)
        except Exception as e:
            print(e)
    ruta = "saturar/"

    pl = "plugins/"
    r_fin=home + ruta
    print(r_fin)
    band = os.path.exists(r_fin)
    print(band)
    if band == False:
        try:
            os.mkdir(r_fin)
        except OSError as e:
            if e.errno != errno.EEXIST:
                print("ya existe la carpeta "+r_fin)
        try:
            os.mkdir(r_fin + pl)
        except OSError as e:
            if e.errno != errno.EEXIST:
                print("ya existe la carpeta "+r_fin + pl)

def main():

    pl_conf()
    SATURAR()
    Gtk.main()
    return 0


if __name__ == '__main__':
    main()
