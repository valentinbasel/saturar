#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
import os
import sys
import gi
gi.require_version("Gtk", "3.0")
from zipfile import ZipFile
from gi.repository import Gtk, Gdk
from saturapi import SATURAPI
from pathlib import Path
import locale
from gtkspellcheck import SpellChecker
import platform
#import webbrowser

class PLUGIN(object):

    """Docstring for PLUGIN. """

    def __init__(self, DIALOGOS):
        """TODO: to be defined.

        :con: TODO

        """
        self._os = platform.system()
        self.etiqueta = "Instalador de Plugins"
        self._dialogos = DIALOGOS
        self.band = True
        self.err = ""

    def ejecutar(self,arch):
        """TODO: Docstring for funcion.
        :returns: TODO

        """
        #if self._os == "Linux":
        REP(arch, self._dialogos)
        return True,"Ejecución exitosa"
        #else:
        #    return False, "Esta función solo anda en linux"


class REP(object):

    """Docstring for REPORTE_CODS. """

    def __init__(self, sql, DIALOGOS):

        """TODO: to be defined. """
        self.band_opcion = False
        self.archivo = sql
        self.s = SATURAPI(sql)
        self.id_cod = None
        self.ventana = Gtk.Window()
        self.ventana.set_title("reporte de códigos") 
        self.ventana.set_modal(True)
        self.mensajes = DIALOGOS(self.ventana)
        self.cargador()

        print("cargador de plugins")
    
    def cargador(self):
        """TODO: Docstring for cargador.

        :arg1: TODO
        :returns: TODO

        """
        home = os.path.expanduser("~") + "/.config/"
        ruta = "saturar/plugins/"
        r_fin=home + ruta
        dialog = Gtk.FileChooserDialog(
                    title="Seleccione un archivo SAT", 
                    parent=self.ventana,
                    action=Gtk.FileChooserAction.OPEN
                    )
        dialog.add_buttons(
                            Gtk.STOCK_CANCEL,
                            Gtk.ResponseType.CANCEL,
                            Gtk.STOCK_OPEN,
                            Gtk.ResponseType.OK,)
        filter_pdf = Gtk.FileFilter()
        filter_pdf.set_name("Archivos SAT")
        filter_pdf.add_pattern("*.sat")
        dialog.add_filter(filter_pdf)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            arch = dialog.get_filename()
            
            #self.cargar(arch)
            print(arch)
            self.mensajes.informacion("Cargo el plugin",arch)
            term="cp " + arch + " " +r_fin
            os.system(term)
            with ZipFile(arch, "r") as zip:
                #zip.printdir()
                zip.extractall(r_fin)
            os.system("rm "+ r_fin + "*.sat")
            self.mensajes.informacion("el plugin fue cargado de forma exitosa","")

        elif response == Gtk.ResponseType.CANCEL:
            self.mensajes.error("hubo un error en la carga","")
        dialog.destroy()


