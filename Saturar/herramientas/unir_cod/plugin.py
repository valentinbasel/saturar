#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
import os
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from saturapi import SATURAPI
from pathlib import Path
import locale
from gtkspellcheck import SpellChecker
import platform
#import webbrowser

class PLUGIN(object):

    """Docstring for PLUGIN. """

    def __init__(self, DIALOGOS):
        """TODO: to be defined.

        :con: TODO

        """
        self._os = platform.system()
        self.etiqueta = "unir códigos"
        self._dialogos = DIALOGOS
        self.band = True
        self.err = ""

    def ejecutar(self,arch):
        """TODO: Docstring for funcion.
        :returns: TODO

        """
        #if self._os == "Linux":
        REP(arch, self._dialogos)
        return True,"Ejecución exitosa"
        #else:
        #    return False, "Esta función solo anda en linux"


class REP(object):

    """Docstring for REPORTE_CODS. """

    def __init__(self, sql, DIALOGOS):

        """TODO: to be defined. """
        self.band_opcion = False
        self.archivo = sql
        self.s = SATURAPI(sql)
        self.id_cod = None
        self.ventana = Gtk.Window()
        self.ventana.set_title("reporte de códigos") 
        self.ventana.set_modal(True)
        self.mensajes = DIALOGOS(self.ventana)
        frame_c = Gtk.Frame()
        frame_c.set_label("Nuevo código")
        frame_c.set_shadow_type(1)

        vbox_general= Gtk.VBox()
        vbox = Gtk.VBox()
###############################################################################
        #self.padre = padre
        #self.id_prop = id_prop 
        self.s = SATURAPI(sql)
        self.archivo = sql
        #self.con 
        self._id_cod = 0

        hbox = Gtk.HBox()
        hbox_L = Gtk.VBox()
        hbox_C = Gtk.VBox()
        f_memo = Gtk.Frame(label="Memo")
        scrolltext = Gtk.ScrolledWindow()
        scrolltext.set_hexpand(True)
        scrolltext.set_vexpand(True)

        vbox_main = Gtk.VBox()
        L_nombre_cod = Gtk.Label("Nombre Código")
        self.E_nombre_cod = Gtk.Entry()
        L_nombre_col = Gtk.Label("color del Código")
        self.E_nombre_col = Gtk.ColorButton()
        gcol = Gdk.color_parse("#FF0000")
        self.E_nombre_col.set_color(gcol)
        ######################################################################
        #  configuraciones del textview
        ######################################################################
        # Widget con el textview donde se visualizan el texto a codificar
        self.pagina = Gtk.TextView()
        spellchecker = SpellChecker(self.pagina,
                                    locale.getdefaultlocale()[0])
        # Corto el texto en las palabras si se pasan del tamaño del textview
        self.pagina.set_wrap_mode(Gtk.WrapMode.WORD)
        self.pagina.set_tooltip_text("")
        # self.pagina.connect("button-press-event", self.boton_mouse)
        self.p_buffer = self.pagina.get_buffer()
        # codigo hardcodeado para probar
        self.p_buffer.set_text("")
        scrolltext.add(self.pagina)
        hbox_L.pack_start(L_nombre_cod, True, True, 0)
        hbox_L.pack_start(L_nombre_col, True, True, 0)

        hbox_C.pack_start(self.E_nombre_cod, True, True, 0)
        hbox_C.pack_start(self.E_nombre_col, True, True, 0)

        hbox.pack_start(hbox_L, True, True, 10)
        hbox.pack_start(hbox_C, True, True, 10)
        vbox_main.pack_start(hbox, False, True, 0)
        f_memo.add(scrolltext)
        vbox_main.pack_start(f_memo, True, True, 0)

        #boton_aceptar = Gtk.Button("Aceptar")

        #boton_cancelar = Gtk.Button("Cancelar")
        #boton_cancelar.connect("clicked", self.cancelar)
        #hbox_b = Gtk.HBox()
        #hbox_b.pack_start(boton_cancelar, True, True, 0)
        #hbox_b.pack_start(boton_aceptar, True, True, 0)
        #vbox_main.pack_start(hbox_b, True, True, 0)
        #if self.editar is not False:
        #    self.__edicion(editar)
        #    boton_aceptar.connect("clicked", self.aceptar_ed)
        #else:

            #boton_aceptar.connect("clicked", self.aceptar)
        frame_c.add(vbox_main)
        vbox_general.pack_start(frame_c,True,True,0)
        #self.ventana.show_all()



############################################################################### 
        cod_cargados = self.cargar_cod()
        frame_nuevos_c = Gtk.Frame()
        frame_nuevos_c.set_label("Códigos a unir")
        frame_nuevos_c.set_shadow_type(1)
        hbox1 = Gtk.HBox()
        drp_cod = Gtk.ComboBox.new_with_model_and_entry(cod_cargados)
        drp_cod.set_entry_text_column(1)
        boton_mas = Gtk.Button("+")
        boton_mas.connect("clicked",self.mas)
        hbox1.pack_start(drp_cod, True, True, 0)
        hbox1.pack_start(boton_mas, True, True, 0)
        self.tex_cod = Gtk.Entry()
        b_aceptar = Gtk.Button("Aceptar")
        b_aceptar.connect("clicked", self.aceptar)
        b_cancelar = Gtk.Button("Cancelar")
        b_cancelar.connect("clicked", self.cancelar)
        b_opcion = Gtk.CheckButton(label="Eliminar códigos viejos")
        b_opcion.connect("toggled", self.b_opcion_toggled)
        drp_cod.connect("changed", self.cambio)
        vbox.pack_start(hbox1, True, True, 0)
        vbox.pack_start(self.tex_cod, True, True, 0)

        hbox_bot = Gtk.HBox()
        hbox_bot.pack_start(b_aceptar, True, True, 0)
        hbox_bot.pack_start(b_cancelar, True, True, 0)

        vbox.pack_start(b_opcion, True, True, 0)
        vbox.pack_start(hbox_bot, True, True, 0)
        frame_nuevos_c.add(vbox)
        vbox_general.pack_start(frame_nuevos_c, True, True, 0)
        self.ventana.add(vbox_general)
        self.ventana.show_all()
        self.dialogos = DIALOGOS
    
    def b_opcion_toggled(self, button):
        """TODO: Docstring for b_opcion_toggled.
        :returns: TODO

        """
        if button.get_active():
            print("borro códigos viejos")
            self.band_opcion = True
            print(self.band_opcion)
        else: 
            self.band_opcion = False
            print("no borro")
            print(self.band_opcion)

    def mas(self, widget):
        """TODO: Docstring for mas.
        :returns: TODO

        """
        text = self.tex_cod.get_text()
        text = text + self.id_cod + ","
        self.tex_cod.set_text(text)


    def aceptar(self, widget):
        """TODO: Docstring for aceptar.
        id_cod
        nombre_cod
        color
        memo
        id_prop
        fecha_alt
        status
        """

        text = self.tex_cod.get_text()
        lista = text.split(",")
        list_limp = set(list(filter(None,lista)))
        #print(len(list_limp))
        if len(list_limp) == 0:
            self.mensajes.error(texto="Error.",
                               subtexto="no hay ningun código agregado")
            return False
        else:
            codigos_n=[]
            for codif in list_limp:
                #print(codif)
                cod_rec = self.s.Recuperar.Codificado(codif)
                for c in cod_rec:
                    codigos_n.append(c)
            #print(codigos_n)
            
            col = self.E_nombre_col.get_rgba().to_string()
            col = col.strip("rgba()")
            col = col.split(",")
            t_col = (int(col[0]), int(col[1]), int(col[2]))
            color = '#%02x%02x%02x' % t_col
            #fecha_alt = time.strftime("%d/%m/%y"),
            status = 1
            id_prop = 1 #self.id_prop
            nombre_cod = self.E_nombre_cod.get_text()
            if nombre_cod == "":
                self.mensajes.error(texto="Error.",
                               subtexto="debes completar algun nombre")
                return False
            memo = self.p_buffer.get_text(self.p_buffer.get_start_iter(),
                                          self.p_buffer.get_end_iter(), True)

            nuevo_codigo = self.s.Agregar.codigo(nombre_cod,color,memo)

            if self.band_opcion == True:
                for cod in list_limp:
                    self.s.Borrar.codigo(cod)
            for dato in codigos_n:
                #id_cod,id_prop,id_pagina,text_cod,c_ini,c_fin)
                self.s.Agregar.codificado(nuevo_codigo,
                                          dato["id_prop"],
                                          dato["id_pagina"],
                                          dato["text_cod"],
                                          dato["c_ini"],
                                          dato["c_fin"])
            self.ventana.hide()
            return True

        print("uno los cod")

    def cancelar(self, widget):
        """TODO: Docstring for cancelar.
        :returns: TODO

        """
        print("cancelo")
        self.ventana.hide()

    def cargar_cod(self):
        """TODO: Docstring for cargar_cod.
        :returns: TODO

        """
        name_store = Gtk.ListStore(str, str)
        codigos = self.s.Recuperar.Codigos()
        for c in codigos:
            name_store.append([c['id_cod'],c['nombre_cod']])

        return name_store
    
    def cambio(self, widget):
        """TODO: Docstring for cambio.
        :returns: TODO

        """
        tree_iter = widget.get_active_iter()
        if tree_iter is not None:
            model = widget.get_model()
            id = model[tree_iter][0]
            #print("Selected: country=%s" % country)
            self.id_cod = id

