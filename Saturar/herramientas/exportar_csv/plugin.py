#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
import os
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from saturapi import SATURAPI
from pathlib import Path
import platform
import pandas as pd
import importlib
import subprocess
import sys

class PLUGIN(object):

    """Plantilla generica de plugins para saturar """

    def __init__(self, DIALOGOS):
        """TODO: to be defined.

        :con: TODO

        """
        self._os = platform.system()
        self.etiqueta = "exportar tablas" # el nombre que se vera en el menu
        self._dialogos = DIALOGOS
        self.band = True
        self.err = ""
        librerias = ["xlwt"]
        self.comprobar_plugin(librerias)

    def ejecutar(self,arch):
        """TODO: Docstring for funcion.
        :returns: TODO

        """
        #if self._os == "Linux":
        REP(arch, self._dialogos)
        return True,"Ejecución exitosa"
        #else:
        #    return False, "Esta función solo anda en linux"

    def comprobar_plugin(self,librerias):
        for l in librerias:
            try:
                # Intentar importar la librería
                importlib.import_module(l)
                print(f"La librería '{l}' ya está instalada.")
            except ImportError:
                # Si la importación falla, instalar la librería
                print(f"La librería '{l}' no está instalada. Instalando...")
                subprocess.check_call([sys.executable, "-m", "pip", "install", l])
                print(f"La librería '{l}' ha sido instalada.")


class REP(object):

    """Docstring for REPORTE_CODS. """

    def __init__(self, sql, DIALOGOS):
        """TODO: to be defined. """
        self.tipo = "csv" # por defecto csv
        self.tabla = "1"
        self.arch = False # para guardar la dirección de un archivo
        self.archivo = sql
        self.s = SATURAPI(sql)
        self.ventana = Gtk.Window()
        self.ventana.set_title("Exportar tablas")
        vbox = Gtk.VBox()
        tablabox = Gtk.VBox()
        #label_op_tabla = Gtk.Label("prueba")

        frame1 = Gtk.Frame(label="Tipo de tabla a exportar") 
        button_tabla = Gtk.RadioButton.new_with_label_from_widget(None, "Exportar tabla con resumen de páginas y códigos.")
        button_tabla.connect("toggled", self.on_button_toggled, "1")
        tablabox.pack_start(button_tabla,True,True,0)
        button_tabla_cod = Gtk.RadioButton.new_from_widget(button_tabla)
        button_tabla_cod.set_label("Exportar tabla de códificados.")
        button_tabla_cod.connect("toggled", self.on_button_toggled, "2")
        tablabox.pack_start(button_tabla_cod, False, False, 0)
        frame1.add(tablabox)
        frame2 = Gtk.Frame(label="Formato del archivo a exportar") 
        tablabox_arch = Gtk.HBox()
        button_tabla_csv = Gtk.RadioButton.new_with_label_from_widget(None, "csv")
        button_tabla_csv.connect("toggled", self.on_button_toggled_arch, "csv")
        tablabox_arch.pack_start(button_tabla_csv,True,True,0)
        
        button_tabla_xls = Gtk.RadioButton.new_from_widget(button_tabla_csv)
        button_tabla_xls.set_label("Excel")
        button_tabla_xls.connect("toggled", self.on_button_toggled_arch, "excel")
        tablabox_arch.pack_start(button_tabla_xls, False, False, 0)
        frame2.add(tablabox_arch)
        b_aceptar = Gtk.Button("Aceptar")
        b_aceptar.connect("clicked", self.aceptar) 
        

        vbox.pack_start(frame1,True,True,0)
        vbox.pack_start(frame2,True,True,0)
        vbox.pack_start(b_aceptar, True, True, 0)
        self.ventana.add(vbox)
        self.ventana.show_all()
        self.dialogos = DIALOGOS
        self.mensajes = DIALOGOS(self.ventana)


    def on_button_toggled_arch(self,widget,a):
        self.tipo = a


    def on_button_toggled(self,widget,a):
        self.tabla = a

    def aceptar(self, widget):
        """TODO: Docstring for aceptar.
        :returns: TODO

        """

        #self.mensajes.informacion("es una prueba","algo")
        self.abrir(widget)
        #print("este es el archivo que busque: ",self.arch)
        self.ventana.hide()

    def abrir(self, widget):
        """TODO: Docstring for nuevo_proyecto.

        :widget: TODO
        :returns: TODO

        """
        dialog = Gtk.FileChooserDialog(
                    title="Seleccione un archivo", 
                    parent=self.ventana,
                    action=Gtk.FileChooserAction.SAVE
                    )
        dialog.add_buttons(
                            Gtk.STOCK_CANCEL,
                            Gtk.ResponseType.CANCEL,
                            Gtk.STOCK_SAVE,
                            Gtk.ResponseType.OK,)
        #filter_pdf = Gtk.FileFilter()
        #filter_pdf.set_name("Archivos sqlite")
        #filter_pdf.add_pattern("*.pdf")
        #dialog.add_filter(filter_pdf)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            arch = dialog.get_filename()
            self.arch = arch
            self.cargar_tabla(self.arch)
            #codificado = self.s.Recuperar.Todo_Codificado()
            #df_cod = pd.DataFrame(codificado)
            #df_cod.to_excel(self.arch)

        elif response == Gtk.ResponseType.CANCEL:
            self.arch = False
        dialog.destroy()

    def cargar_tabla(self,arch):
        codigos = self.s.Recuperar.Codigos()
        paginas = self.s.Recuperar.Paginas()
        tabla_f=[]
        codificado = self.s.Recuperar.Todo_Codificado()
        df = None
        #print(df_cod.head())
        if self.tabla=="1":
            for p in paginas:
                tabla = []
                tabla_cabecera = []
                tabla_cabecera.append("nombre página")
                tabla.append(p["titulo"])
                for c in codigos:
                    num_c=0
                    for cc in codificado:
                        if int(cc["id_pagina"]) == int(p["id_pagina"]) and int(cc["id_cod"]) == int (c["id_cod"]):
                            num_c +=1
                    tabla_cabecera.append(c["nombre_cod"])
                    tabla.append(num_c)
                tabla_f.append(tabla)
            df = pd.DataFrame(tabla_f,columns=tabla_cabecera)
            #print(df.head())
        if self.tabla == "2":
            tabla_codificado = []
            for cc in codificado:
                cod_codif = self.s.Recuperar.Codigos()
                for c_codif in cod_codif:
                    if str(cc["id_cod"])== str(c_codif["id_cod"]):
                        nom_cod = c_codif["nombre_cod"]
                cod_pag = self.s.Recuperar.Paginas()
                for c_pag in cod_pag:
                    if str(c_pag["id_pagina"]) == str(cc["id_pagina"]):
                        nom_pag = c_pag["titulo"]
                dicc_codificado = { "nombre cod" : nom_cod,
                                    "titulo página" : nom_pag,
                                    "texto" : cc["text_cod"],
                                   "fecha" : cc["fecha_creac"]
                                    }
                #print(dicc_codificado)

                tabla_codificado.append(dicc_codificado)

            df = pd.DataFrame(tabla_codificado)

        if self.tipo == "excel":
            df.to_excel(arch+".xls")
        if self.tipo== "csv":
            df.to_csv(arch+".csv")
