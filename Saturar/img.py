#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# manejador y codificador de archivos multimedias (video/audio)
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################


from gettext import gettext as _
#import vlc
import sys
import sqlite3
import gi
import time
import os
from .anotador_img import ANOTADOR
import locale
from gtkspellcheck import SpellChecker
from PIL import Image 
from .utils import DIALOGOS
from operator import itemgetter
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GdkPixbuf, PangoCairo, Pango
import cairo


class tabla_codificado(Gtk.HBox):

    def __init__(self, archivo_db, id_pag):
        super(tabla_codificado, self).__init__()
        self.con = archivo_db

        self.tabla = Gtk.TreeView()
        self.hbox = Gtk.HBox()
        self.id_pag = id_pag
        self.crear_tabla()

        self.add(self.hbox)
        self.show_all()

    def on_tree_selection_changed(self, selection):
        model, treeiter = selection.get_selected()
        self.tabla.set_tooltip_text("")
        if treeiter is not None:
            self._tabla_valor = model[treeiter][0]
            self.seg = model[treeiter][1]

            # Con esto podemos sacar el memo del código y ponerlo en un tooltip
            cod = 'SELECT text_cod,\
                    memo FROM codificacion \
                    WHERE id_codificacion = '+str(self._tabla_valor)
            cursorObj = self.con.cursor()
            cursorObj.execute(cod)
            res = cursorObj.fetchall()
            cadena = "trancripción: \n"
            cadena = cadena + res[0][0] + "\n memo: \n"+res[0][1] 
            self.tabla.set_tooltip_text(cadena)

    def crear_tabla(self):
        """TODO: Docstring for crar_tabla.
        :returns: TODO

        """
        ######################################################################
        # Configuraciones de la tabla
        ######################################################################
        # creamos la lista de almacenamienta
        # variable para guardar el valor del codigo elegido en la tabla

        self.cod_data = self.actualizar_cod(self.id_pag)
        self._tabla_valor = ""
        self.store = Gtk.ListStore(str, str, str, str,str,str)
        for i in self.cod_data:
            self.store.append([i['id_cod'],
                              str(i['c_ini']),
                              str(i['c_fin']),
                              i['text_cod'],
                              i['memo'],
                              i['color']])
        self.tabla.set_model(self.store)
        # definimos las columnas
        col_cod_id = Gtk.TreeViewColumn("ID",
                                     Gtk.CellRendererText(),
                                     text=0,
                                     background=5)
        self.tabla.append_column(col_cod_id)
        col_cod = Gtk.TreeViewColumn("inicio",
                                     Gtk.CellRendererText(),
                                     text=1,
                                     background=5)
        self.tabla.append_column(col_cod)
        col_cod = Gtk.TreeViewColumn("final",
                                     Gtk.CellRendererText(),
                                     text=2,background=5)
        self.tabla.append_column(col_cod)
        col_cod = Gtk.TreeViewColumn("transcripción",
                                     Gtk.CellRendererText(),
                                     text=3,background=5)
        self.tabla.append_column(col_cod)
        self.scrollable_treelist = Gtk.ScrolledWindow()
        self.scrollable_treelist.set_vexpand(True)
        self.hbox.pack_start(self.scrollable_treelist,True,True,0)
        self.scrollable_treelist.add(self.tabla)
        self.seg = 0
        tree_selection = self.tabla.get_selection()
        tree_selection.connect("changed", self.on_tree_selection_changed)

    def actualizar_tabla(self, widget):
        for col in self.tabla.get_columns():
            self.tabla.remove_column(col)
        #self.hbox.remove(self.scrollable_treelist)
        self.crear_tabla()

    def actualizar_cod(self, id_pag):
        """TODO: Docstring for actualizar_cod.
        :returns: TODO

        """
        cod = 'SELECT id_codificacion,\
                id_cod,text_cod,c_ini,c_fin,status,\
                memo FROM codificacion WHERE id_pagina = '+str(id_pag)
        cursorObj = self.con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        cod_data = []
        for r in res:
            if r[5] == 1:
                cod1 = 'SELECT color FROM codigo WHERE id_cod = '+str(r[1])
                cursorObj1 = self.con.cursor()
                cursorObj1.execute(cod1)
                res1 = cursorObj1.fetchall()
                tag = {'id_cod': str(r[0]),
                       'text_cod': r[2],
                       'c_ini': r[3],
                       'c_fin': r[4],
                       'memo': r[6],
                       'color': res1[0][0]}
                cod_data.append(tag)
        cod_data_ord = sorted(cod_data, key=itemgetter('c_ini')) 
        return cod_data_ord



class tabla_cod(Gtk.HBox):

    def __init__(self, archivo_db):
        super(tabla_cod, self).__init__()
        self.con = archivo_db
        self.cod_data = self.actualizar_cod(1)
        self.id_tabla = 0
        self.hbox = Gtk.HBox()
        self.crear_tabla()
        self.add(self.hbox)
        self.show_all()

    def crear_tabla(self):
        """TODO: Docstring for crar_tabla.
        :returns: TODO

        """
        ######################################################################
        # Configuraciones de la tabla
        ######################################################################
        # creamos la lista de almacenamienta
        # variable para guardar el valor del codigo elegido en la tabla
        self._tabla_valor = ""
        self.store = Gtk.ListStore(str, str, str, str)
        for i in self.cod_data:
            self.store.append([i['cod'], i['memo'],  i['b'], i['id_cod']])
            color = Gdk.RGBA()
            color.parse(i['b'])
        self.tabla = Gtk.TreeView(self.store)
        # definimos las columnas
        col_cod_id = Gtk.TreeViewColumn("ID",
                                     Gtk.CellRendererText(),
                                     text=3,
                                     background=2)
        self.tabla.append_column(col_cod_id)
        col_cod = Gtk.TreeViewColumn("codigos",
                                     Gtk.CellRendererText(),
                                     text=0,
                                     background=2)
        self.tabla.append_column(col_cod)

        self.scrollable_treelist = Gtk.ScrolledWindow()
        self.scrollable_treelist.set_vexpand(True)
        self.hbox.pack_start(self.scrollable_treelist,True,True,0)

        self.scrollable_treelist.add(self.tabla)
        self.seg = 0
        tree_selection = self.tabla.get_selection()
        tree_selection.connect("changed", self.on_tree_selection_changed)

    def on_tree_selection_changed(self, selection):

        model, treeiter = selection.get_selected()
        if treeiter is not None:
            self.id_tabla = model[treeiter][3]
            print(self.id_tabla)

    def actualizar_tabla(self, widget):
        for col in self.tabla.get_columns():
            self.tabla.remove_column(col)
        self.crear_tabla()

    def actualizar_cod(self, id_prop):
        """TODO: Docstring for actualizar_cod.
        :returns: TODO

        """
        cod = 'SELECT * FROM codigo WHERE id_prop = '+str(id_prop)
        cursorObj = self.con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        cod_data = []
        for r in res:
            if r[6] == 1:
                tag = {'id_cod': str(r[0]),
                       'cod': r[1],
                       'memo': r[3],
                       'b': r[2]}
                cod_data.append(tag)
        return cod_data



class IMG:
    """Example simple video player.
    """

    def __init__(self, 
                 archivo_db,
                 id_prop,
                 id_pag,
                 ventana,
                 arch_saturar,
                 ventana_central):

        self._id_pag = id_pag
        self._id_prop = id_prop
        self.con = archivo_db
        self.ventana = ventana
        self.__ventana_central = ventana_central
        self.__arch_saturar =arch_saturar 

    def main(self):
        """
        En main esta el arbol de widget que construye la vantana principal
        de Burrhus

        w
        |
         --> hbox
              |
               --> frame
                    |
                     --> boxx
                    |      |
                    |       --> self.vlc
                    |
                     --> vbox
                          |
                           --> self.notebook_tree
                          |
                           --> boton
        """

        win = self.ventana # Gtk.Window(title="video")
        self.mensajes = DIALOGOS(self.__ventana_central)
        hpaned = Gtk.HPaned()
        frame = Gtk.Frame(label="imagen")
        cod_tam = ('SELECT contenido FROM pagina WHERE id_pagina = '\
                   + str(self._id_pag))
        cursorObj = self.con.cursor()
        cursorObj.execute(cod_tam)
        tam = cursorObj.fetchall()
        fimg=tam[0][0]
        print(fimg)
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        # ~ frame = Gtk.Frame()
        # ~ frame.set_label("Contenido de la página")
        # ~ frame.set_shadow_type(1) 
        self.drawing_area = Gtk.DrawingArea()
        self.drawing_area.add_events(Gdk.EventMask.BUTTON_PRESS_MASK) 
        self.drawing_area.connect('draw', self.on_drawing_area_draw)
        self.drawing_area.connect('button-press-event',
                                   self.on_drawing_area_button_press)
        self.drawing_area.connect('motion-notify-event', self.mouse_move)
        self.drawing_area.set_events(self.drawing_area.get_events() |
            Gdk.EventMask.BUTTON_PRESS_MASK |
            Gdk.EventMask.POINTER_MOTION_MASK |
            Gdk.EventMask.BUTTON_RELEASE_MASK)
        # ~ self.drawing_area.set_size_request(800,600)
        self.drawing_area.show()
        frame.add(self.drawing_area)
        scrolledwindow.add(frame)
        box.pack_start(scrolledwindow, True, True, 0)
        # ~ self.ventana.add(box)
        # ~ self.ventana.show_all()
        self.x1 = 0
        self.y1 = 0
        self.band = False
        self.clicks = []
        self._arch = fimg #"/home/vbasel/python/pagina1.png"
        self.ims = cairo.ImageSurface.create_from_png(self._arch)
        h = self.ims.get_height()
        w = self.ims.get_width()
        print("h: ",h)
        print("w: ",w)
        self.drawing_area.set_size_request(w,h)
        self._cod = 0
        self._estado = "baja"

        hbox = Gtk.HBox()
        vbox = Gtk.VBox()
        self.notebook_tree = Gtk.Notebook()
        # ~ boton = Gtk.Button(label="PDI")
        # ~ boton.connect("clicked", self.boton_click)
        boton_srt = Gtk.Button(label="anotar")
        boton_srt.connect("clicked", self.boton_anotar_srt)
        boton_ed = Gtk.Button(label="Editar")
        boton_ed.connect("clicked", self.boton_editar)
        boton_borrar = Gtk.Button(label="borrar")
        boton_borrar.connect("clicked", self.boton_borrar)
        self.pagina_tree = []
        # TABLA con los códigos 
        self.t_cod = tabla_cod(self.con)


        self.notebook_tree.append_page(self.t_cod)
        nt = self.notebook_tree.get_nth_page(0)
        self.notebook_tree.set_tab_label_text(nt, "códigos")
        self.pagina_tree.append(self.t_cod)
        # TABLA con los codificado
        self.t_codific = tabla_codificado(self.con, self._id_pag)


        self.notebook_tree.append_page(self.t_codific)
        nt1 = self.notebook_tree.get_nth_page(1)
        self.notebook_tree.set_tab_label_text(nt1, "transcripciones")
        self.pagina_tree.append(self.t_codific)
        # ~ boxx = Gtk.HBox()
        # ~ boxx.pack_start(box, True, True, 0)
        # ~ frame.add(boxx)
        vbox.pack_start(self.notebook_tree, True, True, 0)
        vbox.pack_start(boton_srt, False, False, 0)
        vbox.pack_start(boton_ed, False, False, 0)
        vbox.pack_start(boton_borrar, False, False, 0)
        # ~ vbox.pack_start(boton, False, False, 0)
        hpaned.add1(box)
        hpaned.add2(vbox)
        win.pack_start(hpaned, True, True, 0)
        win.show_all()
        win.connect("destroy", Gtk.main_quit)
        win.connect("key-press-event", self.on_key_press_event)
        self.actualizar_img()

    def boton_editar(self, arg1):
        """TODO: Docstring for boton_editar.

        :arg1: TODO
        :returns: TODO

        """
        if self.t_codific._tabla_valor != "":
            cod = 'SELECT * FROM codificacion WHERE id_codificacion = '+str(self.t_codific._tabla_valor)
            cursorObj = self.con.cursor()
            cursorObj.execute(cod)
            res = cursorObj.fetchall()
            res = res[0]
            print(res)
            #s = self.vlc.player.get_state()
            #if s != vlc.State.NothingSpecial and self.vlc.player.get_time() > -1:
            #    t = self.vlc.player.get_time()
            #    if s == vlc.State.Playing:
            #        self.vlc.player.pause()
            #    self.anotador = ANOTADOR(self.con,
            #                             self.t_codific,
            #                             self.__arch_saturar,
            #                             editar = True)
            #    self.anotador._id_codificacion = res[0]
            #    self.anotador._id_cod = res[1]
            #    self.anotador._id_prop = res[2]
            #    self.anotador._id_pag = res[3]
            #    self.anotador.p_buffer_trans.set_text(res[4])
            #    self.anotador.entry_inic.set_text(str(res[5]))
            #    self.anotador.entry_fin.set_text(str(res[6]))
            #    self.anotador.p_buffer_memo.set_text(res[9])
            #    self.anotador._player = self.vlc.player
            #    self.anotador.main_windows.show_all()
            

            # esto es para crear el editor nuevo de imagenes 
            #self.drawing_area.queue_draw()
            #self.band = False
            #self.anotador = ANOTADOR(self.con, self.t_codific, self.__arch_saturar,self)
            #self.anotador.x1 = str(self.x1)
            #self.anotador.y1 = str(self.y1)
            #self.anotador.x2 = str(event.x)
            #self.anotador.y2 = str(event.y)         
            #self.anotador._id_codificacion = self._cod
            #self.anotador._id_prop = self._id_prop
            #self.anotador._id_pag = self._id_pag
            #self.anotador._id_cod = self.t_cod.id_tabla
            #self.anotador.main_windows.show_all()
            #self._estado="baja"

        else:
            self.mensajes.error(texto="error",
                                subtexto="no has seleccionado ninguna transcripción")

    def boton_borrar(self, arg1):
        """TODO: Docstring for boton_borrar.

        :arg1: TODO
        :returns: TODO

        """
        c = "cod: " + self.t_codific._tabla_valor
        resp = self.mensajes.pregunta(texto="¿Esta seguro de eliminar el siguiente código?",
                                      subtexto = c)
        if resp== True:
            if self.t_codific._tabla_valor != "":
                cod = 'UPDATE codificacion SET status = 0 where id_codificacion = ' + str(self.t_codific._tabla_valor)
                cursorObj = self.con.cursor()
                cursorObj.execute(cod)
                #res = cursorObj.fetchall()

                self.con.commit()
                #print(res)
                self.t_codific.actualizar_tabla(None)
                self.actualizar_img()
            else:
                self.mensajes.error(texto="error",
                                    subtexto="no has seleccionado ninguna transcripción")
            print("borrar")


    def boton_anotar_srt(self, arg1):
        """TODO: Docstring for boton_anotar_srt.

        :arg1: TODO
        :returns: TODO

        """
        self._estado="alta"



    def on_key_press_event(self, widget, event):
        """TODO: Docstring for on_key_press_event.

        :arg1: TODO
        :returns: TODO

        """
        print("Key val, name: ", event.keyval, Gdk.keyval_name(event.keyval))
        valor_tecla = Gdk.keyval_name(event.keyval)

    def mouse_move(self,  widget, event):
        """TODO: Docstring for mouse_move.

        :arg1: TODO
        :returns: TODO

        """
        cr = cairo.Context(self.ims)
        t=cairo.ImageSurface.create_from_png(self._arch)
        cr.set_source_surface(t, 0, 0)
        cr.paint()
        if self.band == True:
            cr.set_source_rgb(0, 0, 1)
            cr.rectangle(self.x1,self.y1,event.x-self.x1,event.y-self.y1)
            cr.stroke()
            cr.fill()
            self.drawing_area.queue_draw()
            print(event)

    def on_drawing_area_button_press(self, widget, event):
        cr = cairo.Context(self.ims)
        if self._estado == "alta":
            if self.band == False:
                self.x1 = event.x
                self.y1 = event.y
                self.band = True
                return True
            if self.band == True:
                dr = self.drawing_area.get_window()
                # ~ print(self.x1)
                # ~ print(event.x)
                cod_tam = ('SELECT MAX(id_codificacion) FROM codificacion')
                cursorObj = self.con.cursor()
                cursorObj.execute(cod_tam)
                tam = cursorObj.fetchall()
                if tam[0][0] is not None:
                    self._cod = int(tam[0][0]) + 1
                else:
                    self._cod = 1

                # ~ self.clicks.append((self.x1,self.y1,event.x, event.y,self._cod))
                # ~ pixbuf = Gdk.pixbuf_get_from_window(dr,self.x1, self.y1,
                                          # ~ event.x-self.x1,event.y-self.y1)
                # ~ nom = "cod"+str(self._cod)+".png"
                # ~ pixbuf.savev(nom, 'png', [], [])
                # ~ print(self.clicks)
                # ~ self._cod +=1
                if self.x1 > event.x:
                    x_fin = self.x1
                    x_ini = event.x
                else:
                    x_fin = event.x
                    x_ini = self.x1

                if self.y1 > event.y:
                    y_fin = self.y1
                    y_ini = event.y
                else:
                    y_fin = event.y
                    y_ini = self.y1


                self.drawing_area.queue_draw()
                self.band = False
                self.anotador = ANOTADOR(self.con,
                                         self.t_codific,
                                         self.__arch_saturar,
                                         self)
                self.anotador.x1 = str(x_ini)
                self.anotador.y1 = str(y_ini)
                self.anotador.x2 = str(x_fin)
                self.anotador.y2 = str(y_fin) 
                self.anotador._id_codificacion = self._cod
                self.anotador._id_prop = self._id_prop
                self.anotador._id_pag = self._id_pag
                self.anotador._id_cod = self.t_cod.id_tabla
                self.anotador.main_windows.show_all()
                self._estado="baja"
        return True
    def actualizar_img (self):
        """ Function doc """
        cod = 'SELECT * FROM codificacion WHERE status = 1 AND  id_pagina = '+str(self._id_pag)  
        cursorObj = self.con.cursor()
        cursorObj.execute(cod)
        res = cursorObj.fetchall()
        print("****************** actualizamos ****************")
        self.clicks = []
        print(res)
        for dato in res:
            print("fila--")
            print("id: ",dato[0])
            print("xy: ",dato[9])
            cod1 = 'SELECT color FROM codigo WHERE id_cod = '+str(dato[1])
            cursorObj1 = self.con.cursor()
            cursorObj1.execute(cod1)
            res1 = cursorObj1.fetchall()
            xy= dato[9].split("-")
            x1,y1=xy[0].split(",")
            x2,y2=xy[1].split(",")
            print(x1)
            print(y1)
            print(x2)
            print(y2)
            print("color:",res1[0][0])
            self.clicks.append((float(x1),
                               float(y1),
                               float(x2),
                               float(y2),
                               dato[0],
                               res1[0][0]))
        self.draw_mark(self.ims)
        self.drawing_area.queue_draw()

    def on_drawing_area_draw(self, drawing_area, cairo_context):
        cairo_context.set_source_surface(self.ims, 0, 0)
        cairo_context.paint()
        self.draw_mark(self.ims)
        cairo_context.stroke()
        cairo_context.paint()
        return False

    def grabar_img(self, ims):
        """TODO: Docstring for grabar_img.
        :returns: TODO

        """

        img = Image.open(self._arch)
        #dr = self.drawing_area.get_window()
        #print(self.clicks)
        for p in self.clicks:
            img_res = img.crop((p[0], p[1],p[2],p[3]))
            #img_res = img.crop((p[0], p[1],p[2]-p[0],p[3]-p[1]))
            # Write the pixbuf as a PNG image to disk
            #nom = "cod"+str(p[4])+".png"
            dat = os.path.split(self.__arch_saturar)
            arch = dat[0]+ "/img/" +str(p[4])+".png"
            #pixbuf.savev(arch, 'png', [], [])
            img_res.save(arch)

    def draw_mark(self,ims):
        dr = self.drawing_area.get_window()
        cr = cairo.Context(ims)
        tam_font = 7
        FONT = "Sans Bold " + str(tam_font)
        layout = PangoCairo.create_layout (cr)
        desc = Pango.font_description_from_string (FONT)
        layout.set_font_description( desc)
        print(self.clicks)
        for p in self.clicks:
            color = Gdk.RGBA()
            color.parse(p[5])
            cr.set_source_rgba(float(color.red),  
                               float(color.green), 
                               float(color.blue),1)
            cr.rectangle(p[0],p[1],p[2]-p[0],p[3]-p[1])
            cr.move_to(p[0],p[1]-tam_font*2)
            nom = "cod: "+str(p[4])
            layout.set_text(nom, -1)
            PangoCairo.show_layout (cr, layout)
            cr.stroke()

        # self.drawing_area.queue_draw()
