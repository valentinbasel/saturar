#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
import os
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from saturapi import SATURAPI
from pathlib import Path
import platform

import matplotlib.pyplot as plt
import nltk
from nltk import FreqDist
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import pandas as pd 

try:
    import nltk
    nltk.download('punkt_tab')
except Exception as e:
    raise e
    print("no esta instalado las stopword de nltk")

class PLUGIN(object):

    """Plantilla generica de plugins para saturar """

    def __init__(self, DIALOGOS):
        """TODO: to be defined.

        :con: TODO

        """
        self._os = platform.system()
        self.etiqueta = "Contador de frecuencias de palabras" # el nombre que se vera en el menu
        self._dialogos = DIALOGOS
        self.band = True
        self.err = ""
        librerias = []
        self.comprobar_plugin(librerias)

    def ejecutar(self,arch):
        """TODO: Docstring for funcion.
        :returns: TODO

        """
        #if self._os == "Linux":
        REP(arch, self._dialogos)
        return True,"Ejecución exitosa"
        #else:
        #    return False, "Esta función solo anda en linux"

    def comprobar_plugin(self,librerias):
        for l in librerias:
            try:
                # Intentar importar la librería
                importlib.import_module(l)
                print(f"La librería '{l}' ya está instalada.")
            except ImportError:
                # Si la importación falla, instalar la librería
                print(f"La librería '{l}' no está instalada. Instalando...")
                subprocess.check_call([sys.executable, "-m", "pip", "install", l])
                print(f"La librería '{l}' ha sido instalada.")




class REP(object):

    """Docstring for REPORTE_CODS. """

    def __init__(self, sql, DIALOGOS):
        """TODO: to be defined. """
        self.arch = False # para guardar la dirección de un archivo
        self.archivo = sql
        self.id_pag = None
        self.st = True
        self.s = SATURAPI(sql)
        self.ventana = Gtk.Window()
        self.ventana.set_title("Contador de frecuencia de palabras")
        
        vbox = Gtk.VBox()
        ############

        hbox_pag_txt = Gtk.HBox()
        pag_cargados = self.pag_cargados()
        hbox_pag = Gtk.HBox()
        drp_cod = Gtk.ComboBox.new_with_model_and_entry(pag_cargados)
        drp_cod.set_entry_text_column(1)
        boton_mas = Gtk.Button("+")
        boton_mas.connect("clicked",self.mas)
        hbox_pag.pack_start(drp_cod, True, True, 0)
        hbox_pag.pack_start(boton_mas, True, True, 0)
        self.tex_cod = Gtk.Entry()
        l_text_cod = Gtk.Label("ID de las Páginas")
        hbox_pag_txt.pack_start(l_text_cod, False, True, 0)
        hbox_pag_txt.pack_start(self.tex_cod, True, True, 0)
        hbox_stop = Gtk.HBox()

        l_onoff_stop = Gtk.Label("Activar palabras vacias")
        self.stopword = Gtk.Switch()
        self.stopword.set_active(True)
        self.stopword.connect("notify::active",self.cambio_onoff)
        hbox_stop.pack_start(l_onoff_stop,False,False,10)
        hbox_stop.pack_start(self.stopword,False,False,0)
        l_stop = Gtk.Label("Palabras vacias")
        self.e_stop = Gtk.Entry()
        hbox3 = Gtk.HBox()
        hbox3.pack_start(l_stop, False, True, 0)
        hbox3.pack_start(self.e_stop, True, True, 0)
        
        l_cant = Gtk.Label("Cantidad de palabras")
        self.cantid = Gtk.Entry()
        self.cantid.set_text("10")
        hbox4 = Gtk.HBox()
        hbox4.pack_start(l_cant, False, True, 0)
        hbox4.pack_start(self.cantid, True, True, 0)
 

        b_aceptar = Gtk.Button("Aceptar")
        b_aceptar.connect("clicked", self.aceptar)
        drp_cod.connect("changed", self.cambio)

        self.ventana.add(vbox)
        vbox.pack_start(hbox_pag, True, True, 0)
        vbox.pack_start(hbox_pag_txt, True, True, 0) 
        vbox.pack_start(hbox_stop, True, True, 0)
        vbox.pack_start(hbox3, True, True, 0)
        vbox.pack_start(hbox4, True, True, 0)
        vbox.pack_start(b_aceptar, True, True, 0)

        self.ventana.show_all()

        self.dialogos = DIALOGOS
        self.mensajes = DIALOGOS(self.ventana)

    def cambio_onoff(self, switch, gparam):
        if switch.get_active():

            self.st = True
        else:
            self.st = False

        self.e_stop.set_sensitive(self.st)


    def mas(self, widget):
        """TODO: Docstring for mas.
        :returns: TODO

        """
        text = self.tex_cod.get_text()
        text = text + self.id_pag + ","
        self.tex_cod.set_text(text)


    def cambio(self, widget):
        """TODO: Docstring for cambio.
        :returns: TODO

        """
        tree_iter = widget.get_active_iter()
        if tree_iter is not None:
            model = widget.get_model()
            id = model[tree_iter][0]
            self.id_pag = id



    def pag_cargados(self):
        """TODO: Docstring for pag_cargados.

        :widget: TODO
        :returns: TODO

        """
        name_store = Gtk.ListStore(str, str)
        codigos = self.s.Recuperar.Paginas(None)
        for c in codigos:
            name_store.append([c['id_pagina'],c['titulo']])
        return name_store

    def contar_frecuencia_palabras(self,texto):
        palabras = word_tokenize(texto.lower())
        palabras = [palabra for palabra in palabras if palabra.isalpha()]
        palabras_stop = self.e_stop.get_text()
        if self.st==True:
            palabras_stop = palabras_stop.split(",")
            stop_words = set(stopwords.words('spanish')) | set(stopwords.words('english'))
            stop_words.update(palabras_stop)       # Filtrar stopwords
            palabras = [palabra for palabra in palabras if palabra not in stop_words]
        else:
            palabras = [palabra for palabra in palabras]
        frecuencia = FreqDist(palabras)
        return frecuencia


    def aceptar(self, widget):
        """TODO: Docstring for aceptar.
        :returns: TODO

        """
        texto = ""
        palabras_stop = self.e_stop.get_text()
        palabras_stop = palabras_stop.split(",")
        text = self.tex_cod.get_text()
        lista = text.split(",")
        list_limp = set(list(filter(None,lista)))
        print(len(list_limp))
        if len(list_limp) == 0:
            self.mensajes.error(texto="Error.",
                               subtexto="no hay ningun código agregado")
            return False
        else:
            for dato in list_limp:
                pag = self.s.Recuperar.Pagina_Especifica(dato)
                texto =texto + pag[0]['contenido']
                texto = texto.lower()
            ## sigo aca 
            fr = self.contar_frecuencia_palabras(texto)

            cantidad = int(self.cantid.get_text())
            print(fr.most_common(cantidad))
            # Graficar las frecuencias
            palabras, valores = zip(*fr.most_common(cantidad))  # Obtener datos

            plt.bar(palabras, valores, color="skyblue")  # Crear histograma
            plt.xlabel("Palabras")
            plt.ylabel("Frecuencia")
            plt.title("Frecuencia de palabras más comunes")
            plt.xticks(rotation=45)  # Rotar etiquetas para mejor visibilidad

            plt.show()


        #self.mensajes.informacion("es una prueba","algo")
        band1 = self.abrir(widget)
        #print("este es el archivo que busque: ",self.arch)
        print(band1)
        if band1 is not False:
            db_fr = pd.DataFrame(fr.most_common())
            db_fr.to_csv(self.arch+".csv")
        
        self.ventana.hide()

    def abrir(self, widget):
        """TODO: Docstring for nuevo_proyecto.

        :widget: TODO
        :returns: TODO

        """
        dialog = Gtk.FileChooserDialog(
                    title="Seleccione un archivo", 
                    parent=self.ventana,
                    action=Gtk.FileChooserAction.SAVE
                    )
        dialog.add_buttons(
                            Gtk.STOCK_CANCEL,
                            Gtk.ResponseType.CANCEL,
                            Gtk.STOCK_SAVE,
                            Gtk.ResponseType.OK,)
        filter_pdf = Gtk.FileFilter()
        filter_pdf.set_name("Archivos csv")
        filter_pdf.add_pattern("*.csv")
        dialog.add_filter(filter_pdf)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            arch = dialog.get_filename()
            self.arch = arch
        elif response == Gtk.ResponseType.CANCEL:
            self.arch = False
        dialog.destroy()
