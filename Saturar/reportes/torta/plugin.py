#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
import os
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from saturapi import SATURAPI
from pathlib import Path
import platform

##
#if platform.system()=="Linux":
try:
    import pandas as pd
    import matplotlib
    matplotlib.use('GTK3Cairo') #('TKAgg')  # or 'GTK3Cairo'
    import matplotlib.pyplot as plt
except Exception as e:
    print("error")

class PLUGIN(object):

    """Docstring for PLUGIN. """

    def __init__(self, DIALOGOS):
        """TODO: to be defined.

        :con: TODO

        """
        self._os = platform.system()
        self.etiqueta = "Reporte de categorías en grafico de torta"
        self._dialogos = DIALOGOS
        self.band = True
        self.err = ""

    def ejecutar(self,arch):
        """TODO: Docstring for funcion.
        :returns: TODO

        """
        #if self._os == "Linux":
        REP(arch, self._dialogos)
        return True,"Ejecución exitosa"
        #else:
           # return False, "Esta función solo anda en linux"



class REP(object):

    """Docstring for REPORTE_CODS. """
    def __init__(self, sql, DIALOGOS):
        """TODO: to be defined. """
        self.archivo = sql
        self.s = SATURAPI(sql)
        self.id_cod = None
        self.ventana = Gtk.Window()
        self.ventana.set_title("reporte de categorias")
        vbox = Gtk.VBox()
        cat_cargados = self.cargar_cat()
        drp_cod = Gtk.ComboBox.new_with_model_and_entry(cat_cargados)
        drp_cod.set_entry_text_column(1)
        b_aceptar = Gtk.Button("Aceptar")
        b_aceptar.connect("clicked", self.aceptar)
        drp_cod.connect("changed", self.cambio)
        vbox.pack_start(drp_cod, True, True, 0)
        vbox.pack_start(b_aceptar, True, True, 0)
        self.ventana.add(vbox)
        self.ventana.show_all()
        self.dialogos = DIALOGOS

    def make_autopct(self,values):
        def my_autopct(pct):
            total = sum(values)
            val = int(round(pct*total/100.0))
            return '{p:.2f}%  ({v:d})'.format(p=pct,v=val)
        return my_autopct

    def aceptar(self, widget):
        """TODO: Docstring for aceptar.
        :returns: TODO

        """
        c = self.s.Recuperar.Categorizado(self.id_cod)
        cat = []
        for a in c:
            cc = self.s.Recuperar.Codificado(a['id_cod'])
            for p in cc:
                cat.append(p)
        df = pd.DataFrame(cat)
        nombres = []
        colores = []
        for a in c:
            nombres.append(a['nombre'])
            colores.append(a['color'])
        valo = df.value_counts('id_cod')
        plt.pie(valo,autopct=self.make_autopct(valo),colors=colores)
        plt.legend(nombres,loc="best")
        plt.axis('equal')
        plt.show()

    def cargar_cat(self):
        """TODO: Docstring for cargar_cod.
        :returns: TODO

        """
        name_store = Gtk.ListStore(str, str)
        codigos = self.s.Recuperar.Categorias()
        for c in codigos:
            name_store.append([c['id_cat'],c['nombre_cat']])
        return name_store

    def cambio(self, widget):
        """TODO: Docstring for cambio.
        :returns: TODO

        """
        tree_iter = widget.get_active_iter()
        if tree_iter is not None:
            model = widget.get_model()
            id = model[tree_iter][0]
            self.id_cod = id


