#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
#
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
import nltk
#from gensim.models import Word2Vec
#import gensim

# from gensim.models import Word2Vec
import matplotlib.pyplot as plt
#from sklearn.manifold import TSNE
#from sklearn.decomposition import PCA


from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import platform
from pathlib import Path
from saturapi import SATURAPI
from gi.repository import Gtk, Gdk
import os
import gi

gi.require_version("Gtk", "3.0")


# Descargar recursos de NLTK
try:
    import pandas as pd
    import matplotlib

    matplotlib.use("GTK3Cairo")  # ('TKAgg')  # or 'GTK3Cairo'
    import matplotlib.pyplot as plt
except Exception as e:
    print("error")


class PLUGIN(object):
    """Plantilla generica de plugins para saturar"""

    def __init__(self, DIALOGOS):
        """TODO: to be defined.

        :con: TODO

        """
        self._os = platform.system()
        # el nombre que se vera en el menu
        self.etiqueta = "Graficos de torta para códigos (separados por páginas)."
        self._dialogos = DIALOGOS
        self.band = True
        self.err = ""

    def ejecutar(self, arch):
        """TODO: Docstring for funcion.
        :returns: TODO

        """
        # if self._os == "Linux":
        REP(arch, self._dialogos)
        return True, "Ejecución exitosa"
        # else:
        #    return False, "Esta función solo anda en linux"


class REP(object):
    """Docstring for REPORTE_CODS."""

    def __init__(self, sql, DIALOGOS):
        """TODO: to be defined."""
        self.arch = False  # para guardar la dirección de un archivo
        self.archivo = sql
        self.s = SATURAPI(sql)
        self.ventana = Gtk.Window()
        self.ventana.set_title("Grafico de torta por código-página")
        self.id_pag = None
        self.id_cod = None
        self.paginas_cod = []
        vbox = Gtk.VBox()
        hbox_pag_txt = Gtk.HBox()
        hbox_pag_txt_sim = Gtk.HBox()
        hbox_pag = Gtk.HBox()

        pag_cargados = self.pag_cargados()
        drp_pag = Gtk.ComboBox.new_with_model_and_entry(pag_cargados)
        drp_pag.set_entry_text_column(1)

        self.tex_pag = Gtk.Entry()
        boton_mas = Gtk.Button("+")
        boton_mas.connect("clicked", self.mas)
        hbox_pag.pack_start(drp_pag, True, True, 0)
        hbox_pag.pack_start(boton_mas, True, True, 0)

        l_text_pag = Gtk.Label("ID de los pag")
        hbox_pag_txt.pack_start(l_text_pag, False, True, 0)
        hbox_pag_txt.pack_start(self.tex_pag, True, True, 0)
        cod_cargados = self.cargar_cod()
        drp_cod = Gtk.ComboBox.new_with_model_and_entry(cod_cargados)
        drp_cod.set_entry_text_column(1)
        # self.tex_pag_sim = Gtk.Entry()
        # l_text_pag_sim = Gtk.Label("Palabra a buscar")
        # hbox_pag_txt_sim.pack_start(l_text_pag_sim, False, True, 0)
        # hbox_pag_txt_sim.pack_start(self.tex_pag_sim, True, True, 0)

        b_aceptar = Gtk.Button("Aceptar")
        b_aceptar.connect("clicked", self.aceptar)
        drp_pag.connect("changed", self.cambio)

        drp_cod.connect("changed", self.cambio_cod)
        vbox.pack_start(hbox_pag, True, True, 0)
        vbox.pack_start(hbox_pag_txt, True, True, 0)
        # vbox.pack_start(hbox_pag_txt_sim, True, True, 0)

        vbox.pack_start(drp_cod, True, True, 0)

        vbox.pack_start(b_aceptar, True, True, 0)
        self.ventana.add(vbox)
        self.ventana.show_all()
        self.dialogos = DIALOGOS
        self.mensajes = DIALOGOS(self.ventana)

    def aceptar(self, widget):
        """TODO: Docstring for aceptar.
        :returns: TODO

        """
        texto = ""
        text = self.tex_pag.get_text()
        lista = text.split(",")
        list_limp = set(list(filter(None, lista)))
        print(len(list_limp))
        if len(list_limp) == 0:
            self.mensajes.error(
                texto="Error.", subtexto="no hay ningun código agregado"
            )
            return False
        else:
            for dato in list_limp:
                # print("..............", dato)
                # pag = self.s.Recuperar.Pagina_Especifica(int(dato))
                # print(pag)
                # print(self.id_cod)
                self.aplicar(self.id_cod, int(dato))
        df = pd.DataFrame(self.paginas_cod)
        print(df)
        print("--------------------------")
        print(df.keys())
        valor_p = df.value_counts()
        nomb = df.value_counts().rename_axis("unique_values").reset_index(name="counts")
        nomb_cod = self.s.Recuperar.Codigos()
        for nombre_codigos in nomb_cod:
            if nombre_codigos["id_cod"] == self.id_cod:
                codigo_titulo = nombre_codigos["nombre_cod"]

        # ,colors=colores)
        plt.pie(
            valor_p, autopct=self.make_autopct(valor_p), labels=nomb["unique_values"]
        )
        plt.legend(
            nomb["unique_values"],
            loc="best",
        )
        plt.title(label=codigo_titulo, fontdict={"fontsize": 16}, pad=20)
        plt.axis("equal")
        plt.show()

        # ------------------------- aca va el cod para generar el grafico  ------- #

        self.ventana.hide()

    def aplicar(self, id_cod, id_pag):
        c = self.s.Recuperar.Todo_Codificado()
        p = self.s.Recuperar.Paginas()
        cod_f = []

        # print(c)
        # print("------------------------------", id_pag)
        # print("------------------------", id_cod)
        for cod in c:
            if cod["id_cod"] == int(id_cod):
                cod_f.append(cod)
                # print("algo")
        for pag_f in cod_f:
            # print(pag_f)
            if pag_f["id_pagina"] == int(id_pag):
                t = self.s.Recuperar.Pagina_Especifica(int(id_pag))
                print(t)
                titulo = t[0]["titulo"]
                self.paginas_cod.append(titulo)

    # print(self.paginas_cod)

    def make_autopct(self, values):
        def my_autopct(pct):
            total = sum(values)
            val = int(round(pct * total / 100.0))
            return "{p:.2f}%  ({v:d})".format(p=pct, v=val)

        return my_autopct

    def cambio(self, widget):
        """TODO: Docstring for cambio.
        :returns: TODO

        """
        tree_iter = widget.get_active_iter()
        if tree_iter is not None:
            model = widget.get_model()
            id = model[tree_iter][0]
            self.id_pag = id

    def pag_cargados(self):
        """TODO: Docstring for pag_cargados.

        :widget: TODO
        :returns: TODO

        """
        name_store = Gtk.ListStore(str, str)
        codigos = self.s.Recuperar.Paginas(None)
        for c in codigos:
            name_store.append([c["id_pagina"], c["titulo"]])
        return name_store

    def mas(self, widget):
        """TODO: Docstring for mas.
        :returns: TODO

        """
        text = self.tex_pag.get_text()
        text = text + self.id_pag + ","
        self.tex_pag.set_text(text)

    def cargar_cod(self):
        """TODO: Docstring for cargar_cod.
        :returns: TODO

        """
        name_store = Gtk.ListStore(str, str)
        codigos = self.s.Recuperar.Codigos()

        for c in codigos:
            name_store.append([c["id_cod"], c["nombre_cod"]])
        return name_store

    def cambio_cod(self, widget):
        """TODO: Docstring for cambio.
        :returns: TODO

        """
        tree_iter = widget.get_active_iter()
        if tree_iter is not None:
            model = widget.get_model()
            id = model[tree_iter][0]
            self.id_cod = id
