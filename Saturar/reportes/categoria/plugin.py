#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
import os
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
from saturapi import SATURAPI
from pathlib import Path

import platform
import webbrowser

class PLUGIN(object):

    """Docstring for PLUGIN. """

    def __init__(self, DIALOGOS):
        """TODO: to be defined.

        :con: TODO

        """
        self._os = platform.system()
        self.etiqueta = "Reporte de Categorías"
        self._dialogos = DIALOGOS
        self.band = True
        self.err = ""

    def ejecutar(self,arch):
        """TODO: Docstring for funcion.
        :returns: TODO

        """
        #if self._os == "Linux":
        REP(arch, self._dialogos)
        return True,"Ejecución exitosa"
        #else:
        #    return False, "Esta función solo anda en linux"


class REP(object):

    """Docstring for REPORTE_CODS. """

    def __init__(self, sql, DIALOGOS):
        """TODO: to be defined. """
        self.archivo = sql
        self.s = SATURAPI(sql)
        
        self.id_cat = None
        self.ventana = Gtk.Window()
        self.ventana.set_title("reporte de Categorías")
        vbox = Gtk.VBox()
        cod_cargados = self.cargar_cod()
        drp_cod = Gtk.ComboBox.new_with_model_and_entry(cod_cargados)
        drp_cod.set_entry_text_column(1)
        b_aceptar = Gtk.Button("Aceptar")
        b_aceptar.connect("clicked", self.aceptar)
        drp_cod.connect("changed", self.cambio)
        vbox.pack_start(drp_cod, True, True, 0)
        vbox.pack_start(b_aceptar, True, True, 0)
        self.ventana.add(vbox)
        self.ventana.show_all()
        self.dialogos = DIALOGOS

    def aceptar(self, widget):
        """TODO: Docstring for aceptar.
        :returns: TODO

        """
        cods = """## Código {}

### ID: **{}**

---

"""

        mkd_cod = """### Reporte ###

Titulo cuaderno: **{}**

Titulo página: **{}**

tipo de registro: **{}**

fecha de codificación: **{}**


"""

        mkd_text = """Carácter inicial: **{}**

Carácter final: **{}**

### Texto recuperado: ###
{}

### Memo ###

{}

---

"""
        mkd_img = """Milisegundo inicial: **{}**

Milisegundo final: **{}**

### Imagen: ###

<img src="{}" alt=""
	title="" width="400" />

### Transcripción: ###
{}

### Memo ###

{}

---

"""
        mkd_imagen = """### Imagen: ###

<img src="{}" alt=""
	title="" width="400" />

### Transcripción: ###
{}

---

"""
        s = self.s
        arch = ""
        codigos = s.Recuperar.Categorizado(self.id_cat)
        for co in codigos:
            sel_cod = co["id_cod"]
            
            codigos = s.Recuperar.Codigos()
            
            for c in codigos:
                if c['id_cod'] == sel_cod:
                    codi = c['nombre_cod']
                    cid = c['id_cod']
                    arch = arch + cods.format(codi,cid)
            codifi = s.Recuperar.Codificado(sel_cod)
            for a in codifi:
                datos = []
                pag = s.Recuperar.Pagina_Especifica(str(a['id_pagina']))
                cuad = s.Recuperar.Cuaderno_Especifico(str(pag[0]['id_cuaderno']))
                datos.append(str(cuad[0]['nombre_cuad']) )
                datos.append(str(pag[0]['titulo']))
                tipo = str(pag[0]['tipo'])
                datos.append(tipo)
                fech = a['fecha_creac']
                datos.append(fech)
                #datos.append(c_ini)
                #datos.append(c_fin)
                cod_f = mkd_cod.format(*datos)
                texto = a['text_cod']
                #datos.append(texto)
                memo = a['memo']
                #datos.append(texto)
                if tipo == "texto":
                    c_ini = str(a['c_ini'])
                    c_fin = str(a['c_fin'])

                    cod_c = mkd_text.format(c_ini,c_fin,texto,memo)
                elif tipo == "multimedia":
                    c_ini = str(a['c_ini'])
                    c_fin = str(a['c_fin'])
                    direc = os.path.split(self.archivo)
                    im = direc[0] + "/img/" + str(a['id_codificacion'] + ".png")
                    cod_c = mkd_img.format(c_ini,c_fin,im,texto,memo)

                elif tipo == "imagen":
                    direc = os.path.split(self.archivo)
                    im = direc[0] + "/img/" + str(a['id_codificacion'] + ".png")
                    cod_c = mkd_imagen.format(im,texto)
                arch = arch + cod_f + cod_c
        VISOR_REPORTES(arch, self.dialogos)


    def cargar_cod(self):
        """TODO: Docstring for cargar_cod.
        :returns: TODO

        """
        name_store = Gtk.ListStore(str, str)
        codigos = self.s.Recuperar.Categorias()
        for c in codigos:
            name_store.append([c['id_cat'],c['nombre_cat']])

        return name_store
    
    def cambio(self, widget):
        """TODO: Docstring for cambio.
        :returns: TODO

        """
        tree_iter = widget.get_active_iter()
        if tree_iter is not None:
            model = widget.get_model()
            id = model[tree_iter][0]
            #print("Selected: country=%s" % country)
            self.id_cat = id

class VISOR_REPORTES(object):

    """Docstring for VISOR_REPORTES. """

    def __init__(self, texto, DIALOGOS):
        """TODO: to be defined. """
        self.ruta = str(Path.home()) + "/"
        self.ventana = Gtk.Window()
        self.ventana.set_title("Visor de reporte")
        self.ventana.resize(800,600)

        
        self.mensajes = DIALOGOS(self.ventana)
        vbox = Gtk.VBox()

        scrolltext = Gtk.ScrolledWindow()
        scrolltext.set_hexpand(True)
        scrolltext.set_vexpand(True)
        frame = Gtk.Frame()
        #frame.set_label("Resultado")
        frame.set_shadow_type(1)
        hbox = Gtk.HBox()
        ######################################################################
        #  configuraciones del textview
        ######################################################################
        # Widget con el textview donde se visualizan el texto a codificar
        self.pagina = Gtk.TextView()
        # Deshabilito la edición del textview
        self.pagina.set_editable(False)
        # Corto el texto en las palabras si se pasan del tamaño del textview
        self.pagina.set_wrap_mode(Gtk.WrapMode.WORD)
        self.p_buffer = self.pagina.get_buffer()
        self.p_buffer.set_text(texto)
        scrolltext.add(self.pagina)
        frame.add(scrolltext)
        boton_pdf = Gtk.Button("Exportar PDF")
        boton_html = Gtk.Button("Exportar HTML")
        boton_pdf.connect("clicked",self.boton_pdf_aceptar)
        boton_html.connect("clicked",self.boton_html_aceptar)
        hbox.pack_start(boton_pdf, True, True, 0)
        hbox.pack_start(boton_html, True, True, 0)
        vbox.pack_start(frame, True, True, 0)
        vbox.pack_start(hbox, False, False, 0)
        self.ventana.add(vbox)
        self.ventana.show_all()

    def obtener_text(self):
        """TODO: Docstring for obtener_text.
        :returns: TODO

        """
        
        texto = self.p_buffer.get_text(self.p_buffer.get_start_iter(),
                                       self.p_buffer.get_end_iter(),
                                       True)
        return texto

    def boton_html_aceptar(self, widget):
        """TODO: Docstring for boton_pdf_aceptar.
        :returns: TODO

        """

        print("preparo el html")
        texto = self.obtener_text()
        tmp = self.ruta+"temp.md"
        archivo = open(tmp,"w")
        archivo.writelines(texto)
        archivo.close()
        html = "pandoc "+tmp+" -o " +self.ruta+"reporte.html"
        os.system(html)
        html_reporte = self.ruta+"reporte.html"
        subtxt = "el archivo se almaceno en la dirección: " + html_reporte
        self.mensajes.informacion(texto = "el archivo fue creado correctamente",
                                    subtexto = subtxt)
        webbrowser.open(html_reporte)

    def boton_pdf_aceptar(self, widget):
        """TODO: Docstring for boton_pdf_aceptar.
        :returns: TODO

        """
        texto = self.obtener_text()
        tmp = self.ruta + "temp.md"
        archivo = open(tmp, "w")

        archivo.writelines(texto)
        archivo.close()
        html = "pandoc "+tmp+" -o"  +self.ruta+"temp.html"

        os.system(html)
        pdf =  "pandoc " + self.ruta + "temp.html" + " -o ~/reporte.pdf"
        os.system(pdf)
        #os.system("rm ~temp.md")

        pdf_reporte = self.ruta+"reporte.pdf"
        subtxt = "el archivo se almaceno en la dirección: " + pdf_reporte
        self.mensajes.informacion(texto = "el archivo fue creado correctamente",
                                    subtexto = subtxt)
        webbrowser.open(pdf_reporte)
