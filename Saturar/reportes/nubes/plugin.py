#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk
import pathlib
from saturapi import SATURAPI
import platform
from os import path
try:
    from nltk.corpus import stopwords
    from wordcloud import WordCloud
    import pandas as pd 
    try:
        import nltk
        nltk.download('stopwords')
    except Exception as e:
        raise e
        print("no esta instalado las stopword de nltk")
    import matplotlib
    matplotlib.use('GTK3Cairo') #('TKAgg')  # or 'GTK3Cairo'
    import matplotlib.pyplot as plt
    from PIL import Image
    import numpy as np
except Exception as e:
       print("error")

class PLUGIN(object):

    """Docstring for PLUGIN. """

    def __init__(self, DIALOGOS):
        """TODO: to be defined.

        :con: TODO

        """
        self._os = platform.system()
        self.etiqueta = "Reporte Nubes"
        self._dialogos = DIALOGOS
        self.err = ""
        self.band = True
        try: 
            from nltk.corpus import stopwords
            from wordcloud import WordCloud
            import pandas as pd 
            import nltk
            nltk.download('stopwords')
            import matplotlib
            matplotlib.use('TKAgg')  # or 'GTK3Cairo'
            import matplotlib.pyplot as plt
        except Exception as e:
            self.band = False
            print(e)
            self.err = e

    def ejecutar(self,arch):
        """TODO: Docstring for funcion.
        :returns: TODO

        """
        #if self._os == "Linux":
        REP(arch, self._dialogos)
        return True,"Ejecución exitosa"
        #else:
           # return False, "Esta función solo anda en linux"


class REP(object):

    """Docstring for REPORTE_NUBES. """

    def __init__(self, sql,DIALOGOS):
        """TODO: to be defined. """
        self.s = SATURAPI(sql)
        self.archivo = sql
        self.id_pag = None
        self.ventana = Gtk.Window()
        self.ventana.set_title("Nubes de palabras")
        hbox_pag_txt = Gtk.HBox()
        self.mensajes = DIALOGOS(self.ventana)
        vbox = Gtk.VBox()
        pag_cargados = self.pag_cargados()
        hbox_pag = Gtk.HBox()
        drp_cod = Gtk.ComboBox.new_with_model_and_entry(pag_cargados)

        drp_cod.set_entry_text_column(1)
        boton_mas = Gtk.Button("+")
        boton_mas.connect("clicked",self.mas)
        hbox_pag.pack_start(drp_cod, True, True, 0)
        hbox_pag.pack_start(boton_mas, True, True, 0)
        self.tex_cod = Gtk.Entry()
        l_text_cod = Gtk.Label("ID de las Páginas")
        hbox_pag_txt.pack_start(l_text_cod, False, True, 0)
        hbox_pag_txt.pack_start(self.tex_cod, True, True, 0)
        hbox_mascara = Gtk.HBox()
        bto_mascara = Gtk.Button("archivo")

        bto_mascara.connect("clicked",self.bto_mascara_click)
        self.entry_mascara = Gtk.Entry() 
        hbox_mascara.pack_start(bto_mascara, True, True, 5)
        hbox_mascara.pack_start(self.entry_mascara, True, True, 0)
        hbox_color = Gtk.HBox()
        L_nombre_col = Gtk.Label("color del borde")
        self.E_nombre_col = Gtk.ColorButton()
        gcol = Gdk.color_parse("#000000")
        self.E_nombre_col.set_color(gcol)
        hbox_color.pack_start(L_nombre_col,False,True,5)
        hbox_color.pack_start(self.E_nombre_col,True,True,0)
        
        hbox_color_f = Gtk.HBox()
        L_nombre_col_f = Gtk.Label("color del fondo")
        self.E_nombre_col_f = Gtk.ColorButton()
        gcol = Gdk.color_parse("#FFFFFF")
        self.E_nombre_col_f.set_color(gcol)
        hbox_color_f.pack_start(L_nombre_col_f,False,True,5)
        hbox_color_f.pack_start(self.E_nombre_col_f,True,True,0)



        hbox1 = Gtk.HBox()
        l_max_font = Gtk.Label("cantidad maxima de palabras")
        ad1 = Gtk.Adjustment(1, 1, 1000, 1.0, 0, 0)
        self.e_max_font = Gtk.Scale(
            orientation=Gtk.Orientation.HORIZONTAL,
            adjustment=ad1)
        self.e_max_font.set_digits(0)
        hbox1.pack_start(l_max_font, False, True, 5)
        hbox1.pack_start(self.e_max_font, True, True, 0)
        hbox2 = Gtk.HBox()
        l_font_tam = Gtk.Label("Tamaño de la fuente")
        ad2 = Gtk.Adjustment(10, 1, 1000, 1.0, 0, 0)
        self.e_font_tam = Gtk.Scale(
            orientation=Gtk.Orientation.HORIZONTAL,
            adjustment=ad2)
        self.e_font_tam.set_digits(0)
        hbox2.pack_start(l_font_tam, False, True, 0)
        hbox2.pack_start(self.e_font_tam, True, True, 0)
        hbox3 = Gtk.HBox()
        l_stop = Gtk.Label("palabras vacias")
        self.e_stop = Gtk.Entry()
        hbox3.pack_start(l_stop, False, True, 0)
        hbox3.pack_start(self.e_stop, True, True, 0)
        b_aceptar = Gtk.Button("Aceptar")
        b_aceptar.connect("clicked", self.aceptar)
        drp_cod.connect("changed", self.cambio)
        vbox.pack_start(hbox_pag, True, True, 0)
        vbox.pack_start(hbox_pag_txt, True, True, 0)
        vbox.pack_start(hbox_color, True, True, 0)
        vbox.pack_start(hbox_color_f, True, True, 0)
        vbox.pack_start(hbox_mascara, True, True, 0)
        vbox.pack_start(hbox1, True, True, 0)
        vbox.pack_start(hbox2, True, True, 0)
        vbox.pack_start(hbox3, True, True, 0)
        vbox.pack_start(b_aceptar, True, True, 0)
        self.ventana.connect("destroy", self.cerrar)
        self.ventana.add(vbox)
        self.ventana.show_all()
    def bto_mascara_click(self, widget):
        """TODO: Docstring for bto_mascara_click.

        :widget: TODO
        :returns: TODO

        """
        dialog = Gtk.FileChooserDialog("Please choose a file", None,
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        #self.add_filters(dialog)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            print("File selected: " + dialog.get_filename())
            path = dialog.get_filename()

            self.entry_mascara.set_text(path)
            dialog.destroy()
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")
            dialog.destroy()


    def mas(self, widget):
        """TODO: Docstring for mas.
        :returns: TODO

        """
        text = self.tex_cod.get_text()
        text = text + self.id_pag + ","
        self.tex_cod.set_text(text)


    def cerrar(self, arg1):
        """TODO: Docstring for cerrar.

        :arg1: TODO
        :returns: TODO

        """
        print("cierro nube")
        self.ventana.hide()

    def cambio(self, widget):
        """TODO: Docstring for cambio.
        :returns: TODO

        """
        tree_iter = widget.get_active_iter()
        if tree_iter is not None:
            model = widget.get_model()
            id = model[tree_iter][0]
            self.id_pag = id

    def aceptar(self, arg1):
        """TODO: Docstring for aceptar.

        :arg1: TODO
        :returns: TODO

        """
        texto=""
        max_text = int(self.e_max_font.get_value())
        text_tam = int(self.e_font_tam.get_value())
        palabras_stop = self.e_stop.get_text()
        palabras_stop = palabras_stop.split(",")
        text = self.tex_cod.get_text()
        lista = text.split(",")
        list_limp = set(list(filter(None,lista)))
        print(len(list_limp))
        if len(list_limp) == 0:
            self.mensajes.error(texto="Error.",
                               subtexto="no hay ningun código agregado")
            return False
        else:
            for dato in list_limp:
                pag = self.s.Recuperar.Pagina_Especifica(dato)
                texto =texto + pag[0]['contenido']
                texto = texto.lower()
            #self.nube(texto,"nube")
            self.nube_palabras(texto,
                               self.archivo,
                               max_text,
                               text_tam,
                               palabras_stop)

    def nube_palabras(self, text, arch, max_text, text_tam, palabras_stop):
        """TODO: Docstring for nube_palabras.
        :returns: TODO

        """
        stop = set(stopwords.words('spanish')) | set(stopwords.words('english'))
        stop.update(palabras_stop)
        text_f = self.entry_mascara.get_text()
        if text_f!="":
            if path.isfile(text_f)==True:
                mascara = np.array(Image.open(text_f))
            else:
                self.mensajes.error(texto="Error.",
                               subtexto="Archivo no encontrado")
                self.entry_mascara.set_text("")
                return False
        else:
            mascara = None

        col_f = self.E_nombre_col_f.get_rgba().to_string()
        col_f = col_f.strip("rgba()")
        col_f = col_f.split(",")
        t_col_f = (int(col_f[0]), int(col_f[1]), int(col_f[2]))
        color_f = '#%02x%02x%02x' % t_col_f
        
        col = self.E_nombre_col.get_rgba().to_string()
        col = col.strip("rgba()")
        col = col.split(",")
        t_col = (int(col[0]), int(col[1]), int(col[2]))
        color = '#%02x%02x%02x' % t_col
        wordcloud = WordCloud(stopwords=stop,
                              max_font_size=text_tam,
                              max_words=max_text,
                              scale=3,
                              random_state=3,
                              mask= mascara,
                              contour_width=3, 
                              contour_color=color,
                              background_color=color_f).generate(text)
        wordcloud.recolor(random_state=1)

       

        #wordcloud_svg = wordcloud.to_svg()
        #ruta = path.dirname(arch)
        #f = open(ruta + "/nube2.svg", "w+")
        #f.write(wordcloud_svg)
        #f.close()

        plt.figure(figsize=(20, 15))
        #plt.figure()
        plt.imshow(wordcloud, interpolation="bilinear")
        plt.axis("off")
        plt.show()
        #plt.close()



    def pag_cargados(self):
        """TODO: Docstring for pag_cargados.

        :widget: TODO
        :returns: TODO

        """
        name_store = Gtk.ListStore(str, str)
        codigos = self.s.Recuperar.Paginas(None)
        for c in codigos:
            name_store.append([c['id_pagina'],c['titulo']])
        return name_store


#C = REPORTE_NUBES ( "/home/vbasel/Doctorado/owncloud/saturar/doctorado_valen_2020.db")
#Gtk.main()

