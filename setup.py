from setuptools.command.install import install
from setuptools import setup, find_packages

import subprocess

# ~ class InstallLocalPackage(install):
    # ~ def run(self):
        # ~ install.run(self)
        # ~ subprocess.call(
            # ~ "python Saturar/saturapi/setup.py install", shell=True
        # ~ )

#subprocess.call("python saturapi/setup.py install", shell=True)
setup(name="Saturar",  # Nombre
    version="0.3.0",  # Versión de desarrollo
    description="software para analisis QDA",  # Descripción del funcionamiento
    author="Valentín Basel",  # Nombre del autor
    author_email='valentinbasel@gmail.com',  # Email del autor
    license="GPL 3.0",  # Licencia: MIT, GPL, GPL 2.0...
    url="http://satur.ar",  # Página oficial (si la hay)
    #cmdclass={ 'install': InstallLocalPackage },
    packages= [ 'Saturar',
                'Saturar/herramientas/cargador_plugin',
                'Saturar/herramientas/unir_cod',
                'Saturar/herramientas/exportar_csv',
                'Saturar/img',
                'Saturar/reportes/nodos',
               'Saturar/reportes/codigos_paginas',
                'Saturar/reportes/categoria',
                'Saturar/reportes/nubes',
                'Saturar/reportes/pagina',
                'Saturar/reportes/reporte',
                'Saturar/reportes/torta',
                'Saturar/reportes/frecuencia_palabras',
                ],#find_packages(), #['src','src/img','src/reportes'],#
    package_data = { '': ['*.png','*.svg'],},
    #install_requires=[i.strip() for i in open("requirements.txt").readlines()],   
    install_requires=[],
       # 'pycairo',
       # 'numpy',
       # 'pandas',
       # 'pillow',
       # 'wordcloud',
       # 'nltk',
       # 'networkx',
       # 'scipy',
       # 'python-vlc',
       # 'stop-words',
       # 'pygtkspellcheck'
   # ],  
    entry_points={
        'gui_scripts': [
            'saturar = Saturar.main:main'
        ]}
)
